from utils import read_conllu_linear
from utils import read_conllu
from utils import list_to_file
from utils import file_to_list
from utils import read_original_conllu
from config import *
from conllu import parse
import json
import random


def ud_to_sentences(filepath, fileout):
    sentences = read_conllu_linear(filepath)
    sents = []
    for sent in sentences:
        sents.append(sent.metadata['text'])
    list_to_file(sents, fileout)


def ud_to_lemmas(filepath, fileout, lang, form):
    _, sents = read_original_conllu(filepath, lang, form)
    list_to_file(sents, fileout)


def tree_traverse_add_path(root, root_name):
    """
    Construct path-to-node representation for each node and store it to the "other" field in conllu.
    :param root:
    :param root_name:
    :return: tree conllu representation
    """
    lemma = root.token['lemma']
    if lemma == '@card@' or lemma == '@ord@':
        lemma = root.token['form']
        # print('Bad lemma; need to look at', root_name, root)
    if lemma == '_' and root.token['form'] != '_':
        lemma = root.token['form']
    root_name = root_name + '-' + lemma + '-' + root.token['deprel']
    root.token['other'] = root_name
    for child in root.children:
        new_root = tree_traverse_add_path(child, root_name)
    return root


def delexicalise_ud(filepath, part, lang, fileout, dict_path):
    fields = ('id', 'form', 'lemma', 'upostag', 'xpostag', 'feats', 'head', 'deprel', 'deps', 'misc', 'other')
    print(f'Processing {part} in {lang}')
    sentences = read_conllu(filepath)
    sentences_delex = []
    delex_dict = dict_path + 'anonym-path-full-dict-' + lang + '-' + part + '.txt'
    json_dicts = file_to_list(delex_dict)
    for sent, json_dict in zip(sentences, json_dicts):
        path_to_roots_dict = json.loads(json_dict.strip())
        # add info to the "other" field
        sent = tree_traverse_add_path(sent, 'ROOT')
        # print(sent)
        conll_data = sent.serialize()
        assert (len(parse(conll_data)) == 1), "Something wrong with the conllu format."
        sent_delex = []
        for token in parse(conll_data, fields)[0]:
            # print(token)
            path_to_root = token['other']
            anon_lemma = path_to_roots_dict[path_to_root][1]
            sent_delex.append(anon_lemma)
        sentences_delex.append(' '.join(sent_delex))
    # shuffle only training data
    if 'train' in part:
        random.seed(10)
        random.shuffle(sentences_delex)
    list_to_file(sentences_delex, fileout)


def write_target_files():
    out_path = GLOBAL_PATH + '/processed_data/shallow/'
    dict_path = GLOBAL_PATH + '/delex_dicts/'
    # write tgt sentences
    
    # en
    ud_to_sentences(SHALLOW_DEV_UD_EN_1, out_path + 'en/en_1-ud-dev.sent')
    ud_to_sentences(SHALLOW_DEV_UD_EN_2, out_path + 'en/en_2-ud-dev.sent')
    ud_to_sentences(SHALLOW_DEV_UD_EN_3, out_path + 'en/en_3-ud-dev.sent')
    ud_to_sentences(SHALLOW_DEV_UD_EN_4, out_path + 'en/en_4-ud-dev.sent')

    ud_to_sentences(SHALLOW_DEV_UD_FR_1, out_path + 'fr/fr_1-ud-dev.sent')
    ud_to_sentences(SHALLOW_DEV_UD_FR_2, out_path + 'fr/fr_2-ud-dev.sent')
    ud_to_sentences(SHALLOW_DEV_UD_FR_3, out_path + 'fr/fr_3-ud-dev.sent')

    ud_to_sentences(SHALLOW_DEV_UD_ES_1, out_path + 'es/es_1-ud-dev.sent')
    ud_to_sentences(SHALLOW_DEV_UD_ES_2, out_path + 'es/es_2-ud-dev.sent')

    ud_to_sentences(SHALLOW_DEV_UD_KO_1, out_path + 'ko/ko_1-ud-dev.sent')
    ud_to_sentences(SHALLOW_DEV_UD_KO_2, out_path + 'ko/ko_2-ud-dev.sent')

    ud_to_sentences(SHALLOW_DEV_UD_PT_1, out_path + 'pt/pt_1-ud-dev.sent')
    ud_to_sentences(SHALLOW_DEV_UD_PT_2, out_path + 'pt/pt_2-ud-dev.sent')

    ud_to_sentences(SHALLOW_DEV_UD_RU_1, out_path + 'ru/ru_1-ud-dev.sent')
    ud_to_sentences(SHALLOW_DEV_UD_RU_2, out_path + 'ru/ru_2-ud-dev.sent')

    ud_to_sentences(SHALLOW_DEV_UD_JA, out_path + 'ja/ja-ud-dev.sent')
    ud_to_sentences(SHALLOW_DEV_UD_ID, out_path + 'id/id-ud-dev.sent')
    ud_to_sentences(SHALLOW_DEV_UD_HI, out_path + 'hi/hi-ud-dev.sent')
    ud_to_sentences(SHALLOW_DEV_UD_AR, out_path + 'ar/ar-ud-dev.sent')
    ud_to_sentences(SHALLOW_DEV_UD_ZH, out_path + 'zh/zh-ud-dev.sent')
    print('Done writing reference sentences')

    # write tgt token and lemma sequences (from the UD fields token and lemma)
    for form in ['lemma', 'token']:
        if form == 'lemma':
            form_value = False
        else:
            form_value = True
        ud_to_lemmas(SHALLOW_DEV_UD_EN_1, out_path + 'en/en_1-ud-dev.' + form, 'en_1', form=form_value)
        ud_to_lemmas(SHALLOW_DEV_UD_EN_2, out_path + 'en/en_2-ud-dev.' + form, 'en_2', form=form_value)
        ud_to_lemmas(SHALLOW_DEV_UD_EN_3, out_path + 'en/en_3-ud-dev.' + form, 'en_3', form=form_value)
        ud_to_lemmas(SHALLOW_DEV_UD_EN_4, out_path + 'en/en_4-ud-dev.' + form, 'en_4', form=form_value)
    
        ud_to_lemmas(SHALLOW_DEV_UD_FR_1, out_path + 'fr/fr_1-ud-dev.' + form, 'fr_1', form=form_value)
        ud_to_lemmas(SHALLOW_DEV_UD_FR_2, out_path + 'fr/fr_2-ud-dev.' + form, 'fr_2', form=form_value)
        ud_to_lemmas(SHALLOW_DEV_UD_FR_3, out_path + 'fr/fr_3-ud-dev.' + form, 'fr_3', form=form_value)
    
        ud_to_lemmas(SHALLOW_DEV_UD_RU_1, out_path + 'ru/ru_1-ud-dev.' + form, 'ru_1', form=form_value)
        ud_to_lemmas(SHALLOW_DEV_UD_RU_2, out_path + 'ru/ru_2-ud-dev.' + form, 'ru_2', form=form_value)

        ud_to_lemmas(SHALLOW_DEV_UD_KO_1, out_path + 'ko/ko_1-ud-dev.' + form, 'ko_1', form=form_value)
        ud_to_lemmas(SHALLOW_DEV_UD_KO_2, out_path + 'ko/ko_2-ud-dev.' + form, 'ko_2', form=form_value)
    
        ud_to_lemmas(SHALLOW_DEV_UD_PT_1, out_path + 'pt/pt_1-ud-dev.' + form, 'pt_1', form=form_value)
        ud_to_lemmas(SHALLOW_DEV_UD_PT_2, out_path + 'pt/pt_2-ud-dev.' + form, 'pt_2', form=form_value)
    
        ud_to_lemmas(SHALLOW_DEV_UD_ES_1, out_path + 'es/es_1-ud-dev.' + form, 'es_1', form=form_value)
        ud_to_lemmas(SHALLOW_DEV_UD_ES_2, out_path + 'es/es_2-ud-dev.' + form, 'es_2', form=form_value)
    
        ud_to_lemmas(SHALLOW_DEV_UD_AR, out_path + 'ar/ar-ud-dev.' + form, 'ar', form=form_value)
        ud_to_lemmas(SHALLOW_DEV_UD_HI, out_path + 'hi/hi-ud-dev.' + form, 'hi', form=form_value)
        ud_to_lemmas(SHALLOW_DEV_UD_ID, out_path + 'id/id-ud-dev.' + form, 'id', form=form_value)
        ud_to_lemmas(SHALLOW_DEV_UD_JA, out_path + 'ja/ja-ud-dev.' + form, 'ja', form=form_value)
        ud_to_lemmas(SHALLOW_DEV_UD_ZH, out_path + 'zh/zh-ud-dev.' + form, 'zh', form=form_value)
    print('Done writing token and lemma sequences')
    # write tgt delexicalised lemma sequences (from the UD field lemma)

    # en
    delexicalise_ud(SHALLOW_DEV_UD_EN_1, 'dev', 'en_1', out_path + 'en/en_1-ud-dev.delex.lemma', dict_path)
    delexicalise_ud(SHALLOW_DEV_UD_EN_2, 'dev', 'en_2', out_path + 'en/en_2-ud-dev.delex.lemma', dict_path)
    delexicalise_ud(SHALLOW_DEV_UD_EN_3, 'dev', 'en_3', out_path + 'en/en_3-ud-dev.delex.lemma', dict_path)
    delexicalise_ud(SHALLOW_DEV_UD_EN_4, 'dev', 'en_4', out_path + 'en/en_4-ud-dev.delex.lemma', dict_path)

    delexicalise_ud(SHALLOW_TRAIN_UD_EN_1, 'train', 'en_1', out_path + 'en/en_1-ud-train.delex.lemma', dict_path)
    delexicalise_ud(SHALLOW_TRAIN_UD_EN_2, 'train', 'en_2', out_path + 'en/en_2-ud-train.delex.lemma', dict_path)
    delexicalise_ud(SHALLOW_TRAIN_UD_EN_3, 'train', 'en_3', out_path + 'en/en_3-ud-train.delex.lemma', dict_path)
    delexicalise_ud(SHALLOW_TRAIN_UD_EN_4, 'train', 'en_4', out_path + 'en/en_4-ud-train.delex.lemma', dict_path)
    
    # fr
    delexicalise_ud(SHALLOW_DEV_UD_FR_1, 'dev', 'fr_1', out_path + 'fr/fr_1-ud-dev.delex.lemma', dict_path)
    delexicalise_ud(SHALLOW_DEV_UD_FR_2, 'dev', 'fr_2', out_path + 'fr/fr_2-ud-dev.delex.lemma', dict_path)
    delexicalise_ud(SHALLOW_DEV_UD_FR_3, 'dev', 'fr_3', out_path + 'fr/fr_3-ud-dev.delex.lemma', dict_path)

    delexicalise_ud(SHALLOW_TRAIN_UD_FR_1, 'train', 'fr_1', out_path + 'fr/fr_1-ud-train.delex.lemma', dict_path)
    delexicalise_ud(SHALLOW_TRAIN_UD_FR_2, 'train', 'fr_2', out_path + 'fr/fr_2-ud-train.delex.lemma', dict_path)
    delexicalise_ud(SHALLOW_TRAIN_UD_FR_3, 'train', 'fr_3', out_path + 'fr/fr_3-ud-train.delex.lemma', dict_path)

    # es
    delexicalise_ud(SHALLOW_DEV_UD_ES_1, 'dev', 'es_1', out_path + 'es/es_1-ud-dev.delex.lemma', dict_path)
    delexicalise_ud(SHALLOW_DEV_UD_ES_2, 'dev', 'es_2', out_path + 'es/es_2-ud-dev.delex.lemma', dict_path)

    delexicalise_ud(SHALLOW_TRAIN_UD_ES_1, 'train', 'es_1', out_path + 'es/es_1-ud-train.delex.lemma', dict_path)
    delexicalise_ud(SHALLOW_TRAIN_UD_ES_2, 'train', 'es_2', out_path + 'es/es_2-ud-train.delex.lemma', dict_path)

    # ko
    delexicalise_ud(SHALLOW_DEV_UD_KO_1, 'dev', 'ko_1', out_path + 'ko/ko_1-ud-dev.delex.lemma', dict_path)
    delexicalise_ud(SHALLOW_DEV_UD_KO_2, 'dev', 'ko_2', out_path + 'ko/ko_2-ud-dev.delex.lemma', dict_path)

    delexicalise_ud(SHALLOW_TRAIN_UD_KO_1, 'train', 'ko_1', out_path + 'ko/ko_1-ud-train.delex.lemma', dict_path)
    delexicalise_ud(SHALLOW_TRAIN_UD_KO_2, 'train', 'ko_2', out_path + 'ko/ko_2-ud-train.delex.lemma', dict_path)

    # ru
    delexicalise_ud(SHALLOW_DEV_UD_RU_1, 'dev', 'ru_1', out_path + 'ru/ru_1-ud-dev.delex.lemma', dict_path)
    delexicalise_ud(SHALLOW_DEV_UD_RU_2, 'dev', 'ru_2', out_path + 'ru/ru_2-ud-dev.delex.lemma', dict_path)

    delexicalise_ud(SHALLOW_TRAIN_UD_RU_1, 'train', 'ru_1', out_path + 'ru/ru_1-ud-train.delex.lemma', dict_path)
    delexicalise_ud(SHALLOW_TRAIN_UD_RU_2, 'train', 'ru_2', out_path + 'ru/ru_2-ud-train.delex.lemma', dict_path)

    # pt
    delexicalise_ud(SHALLOW_DEV_UD_PT_1, 'dev', 'pt_1', out_path + 'pt/pt_1-ud-dev.delex.lemma', dict_path)
    delexicalise_ud(SHALLOW_DEV_UD_PT_2, 'dev', 'pt_2', out_path + 'pt/pt_2-ud-dev.delex.lemma', dict_path)

    delexicalise_ud(SHALLOW_TRAIN_UD_PT_1, 'train', 'pt_1', out_path + 'pt/pt_1-ud-train.delex.lemma', dict_path)
    delexicalise_ud(SHALLOW_TRAIN_UD_PT_2, 'train', 'pt_2', out_path + 'pt/pt_2-ud-train.delex.lemma', dict_path)

    # ar
    delexicalise_ud(SHALLOW_DEV_UD_AR, 'dev', 'ar', out_path + 'ar/ar-ud-dev.delex.lemma', dict_path)
    delexicalise_ud(SHALLOW_TRAIN_UD_AR, 'train', 'ar', out_path + 'ar/ar-ud-train.delex.lemma', dict_path)

    # id
    delexicalise_ud(SHALLOW_DEV_UD_ID, 'dev', 'id', out_path + 'id/id-ud-dev.delex.lemma', dict_path)
    delexicalise_ud(SHALLOW_TRAIN_UD_ID, 'train', 'id', out_path + 'id/id-ud-train.delex.lemma', dict_path)

    # hi
    delexicalise_ud(SHALLOW_DEV_UD_HI, 'dev', 'hi', out_path + 'hi/hi-ud-dev.delex.lemma', dict_path)
    delexicalise_ud(SHALLOW_TRAIN_UD_HI, 'train', 'hi', out_path + 'hi/hi-ud-train.delex.lemma', dict_path)

    # ja
    delexicalise_ud(SHALLOW_DEV_UD_JA, 'dev', 'ja', out_path + 'ja/ja-ud-dev.delex.lemma', dict_path)
    delexicalise_ud(SHALLOW_TRAIN_UD_JA, 'train', 'ja', out_path + 'ja/ja-ud-train.delex.lemma', dict_path)

    # zh
    delexicalise_ud(SHALLOW_DEV_UD_ZH, 'dev', 'zh', out_path + 'zh/zh-ud-dev.delex.lemma', dict_path)
    delexicalise_ud(SHALLOW_TRAIN_UD_ZH, 'train', 'zh', out_path + 'zh/zh-ud-train.delex.lemma', dict_path)
    print('Done writing delexicalised lemma sequences')
    print(f'All the files were written to {out_path}')


if __name__ == "__main__":
    write_target_files()
