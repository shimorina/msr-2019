Scripts, data, results for the paper

**[_LORIA / Lorraine University at Multilingual Surface Realisation 2019_](https://www.aclweb.org/anthology/D19-6312/). A. Shimorina, C. Gardent. Multilingual Surface Realisation Workshop at EMNLP 2019.** [[poster](msr2019_poster.pdf)]

```
@inproceedings{shimorina-gardent-2019-loria,
    title = "{LORIA} / Lorraine University at Multilingual Surface Realisation 2019",
    author = "Shimorina, Anastasia  and
      Gardent, Claire",
    booktitle = "Proceedings of the 2nd Workshop on Multilingual Surface Realisation (MSR 2019)",
    month = nov,
    year = "2019",
    address = "Hong Kong, China",
    publisher = "Association for Computational Linguistics",
    url = "https://www.aclweb.org/anthology/D19-6312",
    doi = "10.18653/v1/D19-6312",
    pages = "88--93"
}
```

## Requirements
* The code was tested with Python3.6, [OpenNMT-py](https://github.com/OpenNMT/OpenNMT-py) (commit e61589d), and [morphology module](https://github.com/roeeaharoni/morphological-reinflection) from R. Aharoni.

* 	```
	pip install -r requirements.txt
	```

* Download [SR'19 data](http://taln.upf.edu/pages/msr2019-ws/SRST.html#data) and adjust paths in `config.py`
* Download [SR'19 evaluation scripts](http://taln.upf.edu/pages/msr2019-ws/SRST.html#data) and adjust paths in `bash_scripts/config.sh`. The file `eval_Py3_v2.py` was used for the evaluation.

# Surface Realisation Pipeline

```
cd bash_scripts
./run_all.sh
```
This script runs the whole pipeline and does the following.

## Word Ordering
0. Run `delexicalisation_dict_creation.py`. It creates dictionaries for delexicalisation.
1. Run `conll_to_factored.py` It prepares source files for OpenNMT-py.
2. Run `ud_to_string.py`. It prepares target files (delexicalised lemmas, lemmas, tokens, reference sentences).
3. Run `merge_files.sh`. It merges several datasets for one language into one file for training.
4. Run `sr19_factored_fulldelex.sh` to train WO component with OpenNMT-py.
5. Run `sr19_factored_fulldelex_dev.sh` to decode dev data.
5. Run `sr19_factored_fulldelex_test.sh` to decode test data.

## Morphological Realisation + Contraction Generation
0. Run `prepare_morph_for_training.py` and `prepare_morph_based_on_freq.py`. They prepare training files for MR.
1. Run `hard_attent_sr19.sh` for decoding on test data and `hard_attent_decode_sr19.sh` for decoding on dev data. They train MR neural module.
2. Run `create_lemma_form_dicts.py`. It creates morphological dictionaries from MR output.
3. (Optional) `dual_form_analysis.py` identifies lemmas with ambiguous forms (same form and MSF).

## Evaluation
To use the evaluation script, one need to put outputs and references into templates.
The scripts below relexicalise predictions using delexicalisation dictionaries and perform inflection using morphology dictionaries, then outputs are put into templates to produce scores.

0. `generate_templates.sh`. It prepares references in a template form.
* `sr19_factored_fulldelex_lemma_eval.sh`. It evaluates WO against sequences of lemmas.
* `sr19_factored_fulldelex_form_gold_wo_eval.sh`. It evaluates MR against reference sentences. Gold WO files are taken to generate inflected sentences. 
* `sr19_factored_fulldelex_form_eval.sh`. It evaluates WO+MR+CG against reference sentences. 
* `sr19_factored_fulldelex_form_test.sh`. It produces final outputs for test data.


## Repository Structure

* `bash_scripts`

	all the bash scripts

* `delex_dicts`

	Dictionaries for delexicalisation are stored here. There are two types of delexicalisation dicts:
	* `anonym-id-lemma` (maps id to lemma and MSF)
	* `anonym-path-full` (maps full-path-to-node to lemma and id)


* `morphology`

	* `data_for_reinflection` - input data for MR
	* `dual_forms` - ambiguous forms in UD data
	* `lemma_form_dicts` - learned morphological paradigms in json dictionaries
	* `results-sr19` - MR output

* `opennmtpy_models`

	predictions for WO

* `outputs`

	model outputs in templates

* `processed_data`

	* `shallow` - source data
	* `templates` - references

* `turbo_parser_tokeniser`

	Module for contraction generation (adapted from [parsing](https://github.com/andre-martins/TurboParser/tree/f9da3b5333f3780d1698cde5db0d90cca39b4ac6/python/tokenizer); reverted regular expressions were written for fr, it, pt).


## Reproducibility
Tokenisation/contraction modules were changed after the paper submission, so some scores may slightly differ from those reported in the paper.

Also, at that time Arabic contraction generation was not implemented. If you want to reproduce results without contractions, run `deanonymise.py` with the `--no-contract` option in `*eval.sh`.

## Acknowledgements

Thanks to Dr. Ahmed Abdelali for developing Arabic contraction generation module.
