import argparse
import os
import json
from utils import file_to_list
from turbo_parser_tokeniser.universal_contractions import UniversalContractions
import pyarabic.araby as araby
from config import GLOBAL_PATH


def ar_treatment(lemma):
    """Strip vowels from a text, including Shadda."""
    stripped_lemma = araby.strip_tashkeel(lemma)
    return stripped_lemma


def get_inflected_form(lemma, msd, morph_dict, lang):
    # lemma can be uppercase, but morph_dict is only lowercase
    # apply specific preprocessing to match lemmas in morphology dict
    lang_short = lang.split('_')[0] if '_' in lang else lang
    if lang_short == 'ar':
        lemma = ar_treatment(lemma)
    try:
        return morph_dict[lemma][msd]
    except KeyError:
        # copy lemma verbatim to output
        if '#' in lemma and lang_short == 'fi':
            mod_lemma = lemma.replace('#', '')
            try:
                return morph_dict[mod_lemma][msd]
            except KeyError:
                # print(lemma, msd)
                return mod_lemma
        if '_' in lemma and lang_short == 'nl':
            # print(lemma, msd)
            # during morph dict training we stripped off prefixes if it wasn't found in form,
            # so we need to do the same here. If (lemma,form) pair is not in dict, we remove
            # the prefix and try to get the verb form without prefix.
            try:
                lemma = lemma.split('_')[0]
                return morph_dict[lemma][msd]
            except ValueError:
                return lemma.replace('_', '')
        # remove + from lemma
        if lang_short == 'ko':
            lemma = lemma.replace('+', '')
        if lemma == 'COMMA' and lang_short == 'hi':
            lemma = ','  # todo: hi -- now add comma manually
        if 'ru' in lang and lemma[0].isupper():
            try:
                # need to account for USSR
                lemma_lower = morph_dict[lemma.lower()][msd]
                if lemma.isupper():
                    return lemma_lower.upper()
                else:
                    return lemma_lower.capitalize()
            except KeyError:
                return lemma
        return lemma


def deanonymise_predfile(anonym_dict, prediction_file, lang, part, inflect=False, contractions=False):
    """
    Deanonymise a prediction file.
    :param project_path: global path to the project
    :param anonym_dict: .json anonymisation dictionary
    :param prediction_file: .txt prediction file
    :param part: dev or test
    :param lang: language
    :param inflect: replace a lemma by its inflected form
    :param contractions: perform contraction realisation
    :return:
    """
    lang_short = lang.split('_')[0] if '_' in lang else lang
    path = os.path.dirname(prediction_file)
    pred_file_name = os.path.basename(prediction_file)[:-4]
    predictions = file_to_list(prediction_file)
    json_dicts = file_to_list(anonym_dict)
    pred_deanonymised = []

    if inflect:
        # todo: replace path
        inflect_dict = GLOBAL_PATH + '/morphology/lemma_form_dicts/' + lang_short + '-' + part + '-dict.json'
        with open(inflect_dict) as json_file:
            morph_dict = json.load(json_file)

    for line, prediction in zip(json_dicts, predictions):
        line_dict = json.loads(line.strip())
        prediction_deanon = prediction

        # don't need to lowercase with full anonymisation
        for token in prediction.split():
            if token in line_dict.keys():
                lemma, msd = line_dict[token]
                # todo remove lin from msd
                new_msd = [item for item in msd.split(',') if 'lin' not in item]
                msd = ','.join(new_msd)
                if lang_short in ['ja', 'ko', 'zh'] and 'Feats' not in msd:
                    msd = msd + ',Feats=None'  # todo
                if inflect:
                    if lang == 'ru_2':
                        lemma = lemma.lower()
                    word_for_rplc = get_inflected_form(lemma, msd, morph_dict, lang)
                else:
                    word_for_rplc = lemma
                prediction_deanon = prediction_deanon.replace(token, word_for_rplc)
        if lang_short in ['fr', 'it', 'pt', 'ar'] and contractions:
            # contraction realisation
            contraction_splitter = UniversalContractions(language=lang_short)
            prediction_deanon = contraction_splitter.realise_surface_form(prediction_deanon)

        pred_deanonymised.append(prediction_deanon)
    if inflect and not contractions:
        out_file_name = path + '/' + pred_file_name + '.deanon.inflect.txt'
    elif inflect and contractions:
        out_file_name = path + '/' + pred_file_name + '.deanon.inflect.contract.txt'
    else:
        out_file_name = path + '/' + pred_file_name + '.deanon.txt'

    with open(out_file_name, 'w+') as f:
        f.write(''.join(pred_deanonymised))
    print('Deanonymised', pred_file_name)


def parse_args():
    parser = argparse.ArgumentParser(description='Deanonymise prediction file and inflect tokens')
    # anonym_dict, prediction_file, option, lang, inflect=False, contractions=False
    parser.add_argument('anonym_dict', type=str,
                        help='json anonymisation dict')
    parser.add_argument('predfile', type=str,
                        help='prediction file to be processed')
    parser.add_argument('lang', type=str,
                        help='language code (e.g. en, ar, pt, ru)')
    parser.add_argument('-p', '--part', type=str, default='dev', choices=['dev', 'test'],
                        help='inflection dict for dev or test')

    parser.add_argument('--inflect', dest='inflectfeat', action='store_true',
                        help='perform morphological reinflection')
    parser.add_argument('--no-inflect', dest='inflectfeat', action='store_false',
                        help='do not perform morphological reinflection')

    parser.add_argument('--contract', dest='contractfeat', action='store_true',
                        help='perform surface realisation of contractions')
    parser.add_argument('--no-contract', dest='contractfeat', action='store_false',
                        help='do not perform surface realisation of contractions')

    parser.set_defaults(inflectfeat=True, contractfeat=True)
    args_init = parser.parse_args()
    return args_init


if __name__ == '__main__':
    args = parse_args()
    print('Deanonymisation dict', args.anonym_dict)
    print('Processing prediction file', args.predfile)
    deanonymise_predfile(args.anonym_dict, args.predfile, args.lang, args.part, args.inflectfeat, args.contractfeat)


# todo: fr j' je (in morph dict?)
