import random
import json
from io import open
from config import *
from utils import read_conllu
from utils import file_to_list


"""
This code does the following:
- read src conll
- linearise using depth-first
- delexicalise
- create delex dict
- create sequential input to opennmt
- create factored input to opennmt
"""


class IndexMem:

    def __init__(self):
        self.index = 1  # start enumerating nodes with 1


def linearise_sentences(filename, anonym_rootpath_dicts, delexicalise):
    sentences = read_conllu(filename)
    linearised_sents = []
    for sentence, json_dict in zip(sentences, anonym_rootpath_dicts):
        path_to_roots_dict = json.loads(json_dict.strip())
        # find root
        root = sentence.token
        assert root['deprel'] == 'ROOT' or 'root'
        ind_node = IndexMem()
        # parent_id, root name
        linearised_sents.append(depth_first_search(sentence, ind_node, 0, 'ROOT', path_to_roots_dict, delexicalise))
    return linearised_sents


def depth_first_search(node, ind_node, parent_id, root_name, path_to_roots_dict, delexicalise):
    linearised_sent = []
    deprel = node.token['deprel']
    upos = node.token['upostag']
    lemma = node.token['form']   # the second field is lemma

    # add lemma (anonymised as UPOS-deprel-id) as a node
    root_name = root_name + '-' + lemma + '-' + deprel

    # be careful using ￨ (unicode char FFE8) for building factored input
    if ' ' in lemma:
        print('space in lemma was replaced:', lemma, upos, deprel)
        lemma = lemma.replace(' ', '|||||')  # found in fi, fr, and ru
    if '￨' in lemma or '￨' in upos or '￨' in deprel:
        print(lemma, upos, deprel)
        print('the forbidden character is present')
    if delexicalise:
        anon_lemma = path_to_roots_dict[root_name][1]  # the second element is a template
        factored_represent = anon_lemma + '￨' + str(ind_node.index) + '￨' + upos + '￨' + str(parent_id) + '￨' + deprel
    else:
        factored_represent = lemma + '￨' + str(ind_node.index) + '￨' + upos + '￨' + str(parent_id) + '￨' + deprel

    # linearised_sent.append(factored_represent)
    parent_id = str(ind_node.index)
    # story￨2￨NOUN￨1￨A1 -- deep
    # story￨2￨NOUN￨1￨nsubj -- shallow

    lin_orders = []  # a list of (child: lin_order)
    for child in node.children:
        feats = child.token['feats']
        if feats and 'lin' in feats.keys():
            lin_order = int(feats['lin'])
            # print(lin_order)
        else:
            lin_order = 0
        lin_orders.append((child, lin_order))
    # sort dict by values and loop
    children_sorted = sorted(lin_orders, key=lambda tup: tup[1])  # (child, lin_order)
    # iterate through children according to their order and insert parent once done with negatives
    iterator = 0
    while iterator < len(children_sorted) and children_sorted[iterator][1] < 0:
        ind_node.index += 1
        child = children_sorted[iterator][0]
        linearised_sent += depth_first_search(child, ind_node, parent_id, root_name, path_to_roots_dict,
                                              delexicalise)
        iterator += 1
    # insert parent
    linearised_sent.append(factored_represent)
    while iterator < len(children_sorted):
        ind_node.index += 1
        child = children_sorted[iterator][0]
        linearised_sent += depth_first_search(child, ind_node, parent_id, root_name, path_to_roots_dict, delexicalise)
        iterator += 1

    return linearised_sent


def write_data(filename, track, lang, part, dict_path, out_path, delexicalise=True):
    anonym_rootpath_dict = dict_path + '/anonym-path-full-dict-' + lang + '-' + part + '.txt'
    json_dicts = file_to_list(anonym_rootpath_dict)
    linearised_sents = linearise_sentences(filename, json_dicts, delexicalise)
    if delexicalise:
        delex = '.delex'
    else:
        delex = ''
    if '_' in lang:
        lang_short = lang.split('_')[0]
    else:
        lang_short = lang
    write_path = out_path + '/' + track + '/' + lang_short + '/' + lang + '-ud-' +\
                 part + '.' + track + delex + '.5factored'

    # shuffle only training data
    if 'train' in part:
        random.seed(10)
        random.shuffle(linearised_sents)

    with open(write_path, 'w+') as f:
        sentences = []
        for sent in linearised_sents:
            sentences += [' '.join(sent) + '\n']
        f.write(''.join(sentences))
    print(f'Files for {filename} were written to disc.')


def write_all():
    # Output directories
    dict_path = GLOBAL_PATH + '/delex_dicts'
    out_path = GLOBAL_PATH + '/processed_data'
    # Shallow track
    # en
    write_data(SHALLOW_TRAIN_EN_1, 'shallow', 'en_1', 'train', dict_path, out_path)
    write_data(SHALLOW_TRAIN_EN_2, 'shallow', 'en_2', 'train', dict_path, out_path)
    write_data(SHALLOW_TRAIN_EN_3, 'shallow', 'en_3', 'train', dict_path, out_path)
    write_data(SHALLOW_TRAIN_EN_4, 'shallow', 'en_4', 'train', dict_path, out_path)
    write_data(SHALLOW_DEV_EN_1, 'shallow', 'en_1', 'dev', dict_path, out_path)
    write_data(SHALLOW_DEV_EN_2, 'shallow', 'en_2', 'dev', dict_path, out_path)
    write_data(SHALLOW_DEV_EN_3, 'shallow', 'en_3', 'dev', dict_path, out_path)
    write_data(SHALLOW_DEV_EN_4, 'shallow', 'en_4', 'dev', dict_path, out_path)
    write_data(SHALLOW_TEST_EN_1, 'shallow', 'en_1', 'test', dict_path, out_path)
    write_data(SHALLOW_TEST_EN_2, 'shallow', 'en_2', 'test', dict_path, out_path)
    write_data(SHALLOW_TEST_EN_3, 'shallow', 'en_3', 'test', dict_path, out_path)
    write_data(SHALLOW_TEST_EN_4, 'shallow', 'en_4', 'test', dict_path, out_path)
    write_data(SHALLOW_TEST_EN_5, 'shallow', 'en_5', 'test', dict_path, out_path)
    write_data(SHALLOW_TEST_EN_6, 'shallow', 'en_6', 'test', dict_path, out_path)
    write_data(SHALLOW_TEST_EN_7, 'shallow', 'en_7', 'test', dict_path, out_path)

    # es
    write_data(SHALLOW_TRAIN_ES_1, 'shallow', 'es_1', 'train', dict_path, out_path)
    write_data(SHALLOW_TRAIN_ES_2, 'shallow', 'es_2', 'train', dict_path, out_path)
    write_data(SHALLOW_DEV_ES_1, 'shallow', 'es_1', 'dev', dict_path, out_path)
    write_data(SHALLOW_DEV_ES_2, 'shallow', 'es_2', 'dev', dict_path, out_path)
    write_data(SHALLOW_TEST_ES_1, 'shallow', 'es_1', 'test', dict_path, out_path)
    write_data(SHALLOW_TEST_ES_2, 'shallow', 'es_2', 'test', dict_path, out_path)
    write_data(SHALLOW_TEST_ES_3, 'shallow', 'es_3', 'test', dict_path, out_path)

    # fr
    write_data(SHALLOW_TRAIN_FR_1, 'shallow', 'fr_1', 'train', dict_path, out_path)
    write_data(SHALLOW_TRAIN_FR_2, 'shallow', 'fr_2', 'train', dict_path, out_path)
    write_data(SHALLOW_TRAIN_FR_3, 'shallow', 'fr_3', 'train', dict_path, out_path)
    write_data(SHALLOW_DEV_FR_1, 'shallow', 'fr_1', 'dev', dict_path, out_path)
    write_data(SHALLOW_DEV_FR_2, 'shallow', 'fr_2', 'dev', dict_path, out_path)
    write_data(SHALLOW_DEV_FR_3, 'shallow', 'fr_3', 'dev', dict_path, out_path)
    write_data(SHALLOW_TEST_FR_1, 'shallow', 'fr_1', 'test', dict_path, out_path)
    write_data(SHALLOW_TEST_FR_2, 'shallow', 'fr_2', 'test', dict_path, out_path)
    write_data(SHALLOW_TEST_FR_3, 'shallow', 'fr_3', 'test', dict_path, out_path)

    # hi
    write_data(SHALLOW_TRAIN_HI, 'shallow', 'hi', 'train', dict_path, out_path)
    write_data(SHALLOW_DEV_HI, 'shallow', 'hi', 'dev', dict_path, out_path)
    write_data(SHALLOW_TEST_HI_1, 'shallow', 'hi_1', 'test', dict_path, out_path)
    write_data(SHALLOW_TEST_HI_2, 'shallow', 'hi_2', 'test', dict_path, out_path)

    # id
    write_data(SHALLOW_TRAIN_ID, 'shallow', 'id', 'train', dict_path, out_path)
    write_data(SHALLOW_DEV_ID, 'shallow', 'id', 'dev', dict_path, out_path)
    write_data(SHALLOW_TEST_ID, 'shallow', 'id', 'test', dict_path, out_path)

    # ja
    write_data(SHALLOW_TRAIN_JA, 'shallow', 'ja', 'train', dict_path, out_path)
    write_data(SHALLOW_DEV_JA, 'shallow', 'ja', 'dev', dict_path, out_path)
    write_data(SHALLOW_TEST_JA_1, 'shallow', 'ja_1', 'test', dict_path, out_path)
    write_data(SHALLOW_TEST_JA_2, 'shallow', 'ja_2', 'test', dict_path, out_path)

    # ko
    write_data(SHALLOW_TRAIN_KO_1, 'shallow', 'ko_1', 'train', dict_path, out_path)
    write_data(SHALLOW_TRAIN_KO_2, 'shallow', 'ko_2', 'train', dict_path, out_path)
    write_data(SHALLOW_DEV_KO_1, 'shallow', 'ko_1', 'dev', dict_path, out_path)
    write_data(SHALLOW_DEV_KO_2, 'shallow', 'ko_2', 'dev', dict_path, out_path)
    write_data(SHALLOW_TEST_KO_1, 'shallow', 'ko_1', 'test', dict_path, out_path)
    write_data(SHALLOW_TEST_KO_2, 'shallow', 'ko_2', 'test', dict_path, out_path)
    write_data(SHALLOW_TEST_KO_3, 'shallow', 'ko_3', 'test', dict_path, out_path)

    # pt
    write_data(SHALLOW_TRAIN_PT_1, 'shallow', 'pt_1', 'train', dict_path, out_path)
    write_data(SHALLOW_TRAIN_PT_2, 'shallow', 'pt_2', 'train', dict_path, out_path)
    write_data(SHALLOW_DEV_PT_1, 'shallow', 'pt_1', 'dev', dict_path, out_path)
    write_data(SHALLOW_DEV_PT_2, 'shallow', 'pt_2', 'dev', dict_path, out_path)
    write_data(SHALLOW_TEST_PT_1, 'shallow', 'pt_1', 'test', dict_path, out_path)
    write_data(SHALLOW_TEST_PT_2, 'shallow', 'pt_2', 'test', dict_path, out_path)
    write_data(SHALLOW_TEST_PT_3, 'shallow', 'pt_3', 'test', dict_path, out_path)

    # ar
    write_data(SHALLOW_TRAIN_AR, 'shallow', 'ar', 'train', dict_path, out_path)
    write_data(SHALLOW_DEV_AR, 'shallow', 'ar', 'dev', dict_path, out_path)
    write_data(SHALLOW_TEST_AR, 'shallow', 'ar', 'test', dict_path, out_path)

    # ru
    write_data(SHALLOW_TRAIN_RU_1, 'shallow', 'ru_1', 'train', dict_path, out_path)
    write_data(SHALLOW_TRAIN_RU_2, 'shallow', 'ru_2', 'train', dict_path, out_path)
    write_data(SHALLOW_DEV_RU_1, 'shallow', 'ru_1', 'dev', dict_path, out_path)
    write_data(SHALLOW_DEV_RU_2, 'shallow', 'ru_2', 'dev', dict_path, out_path)
    write_data(SHALLOW_TEST_RU_1, 'shallow', 'ru_1', 'test', dict_path, out_path)
    write_data(SHALLOW_TEST_RU_2, 'shallow', 'ru_2', 'test', dict_path, out_path)
    write_data(SHALLOW_TEST_RU_3, 'shallow', 'ru_3', 'test', dict_path, out_path)

    # zh
    write_data(SHALLOW_TRAIN_ZH, 'shallow', 'zh', 'train', dict_path, out_path)
    write_data(SHALLOW_DEV_ZH, 'shallow', 'zh', 'dev', dict_path, out_path)
    write_data(SHALLOW_TEST_ZH, 'shallow', 'zh', 'test', dict_path, out_path)


if __name__ == "__main__":
    write_all()
