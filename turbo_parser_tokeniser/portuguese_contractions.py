# -*- coding: utf-8 -*-

import regex
from turbo_parser_tokeniser.contractions import Contractions


class PortugueseContractions(Contractions):
    def __init__(self):
        # A blacklist of words that should not be confused with contractions.
        # If True, mark consonants removed due to enclitics with symbols # and
        # -CL- for mesoclitics.
        self.mark_enclitics = False
        self.non_contractions = {}
        # self.contractions = self._generate_contractions()
        self.clitics, self.clitic_suffixes = self._generate_clitics()

    def _generate_contractions(self):
        """
        Generate contractions for Portuguese, along with the words
        and lemmas that are contracted (e.g. contraction "das" is composed by
        words "de" + "as", with corresponding lemmas "de" + "o".
        Return a dictionary of contractions, each entry containing a list of
        words and a list of lemmas (typically lists of length two).

        :rtype: dictionary of lists.
        """
        contractions = {}

        contractions[u'ao'] = [u'a', u'o'], [u'a', u'o']
        contractions[u'à'] = [u'a', u'a'], [u'a', u'o']
        contractions[u'aos'] = [u'a', u'os'], [u'a', u'o']
        contractions[u'às'] = [u'a', u'as'], [u'a', u'o']
        contractions[u'àquele'] = [u'a', u'aquele'], [u'a', u'aquele']
        contractions[u'àquela'] = [u'a', u'aquela'], [u'a', u'aquele']
        contractions[u'àqueles'] = [u'a', u'aqueles'], [u'a', u'aquele']
        contractions[u'àquelas'] = [u'a', u'aquelas'], [u'a', u'aquele']
        contractions[u'àquilo'] = [u'a', u'aquilo'], [u'a', u'aquilo']
        contractions[u'aonde'] = [u'a', u'onde'], [u'a', u'onde']
        contractions[u'àqueloutro'] = [u'a', u'aqueloutro'], \
                                      [u'a', u'aqueloutro']
        contractions[u'àqueloutra'] = [u'a', u'aqueloutra'], \
                                      [u'a', u'aqueloutro']
        contractions[u'àqueloutros'] = [u'a', u'aqueloutros'], \
                                       [u'a', u'aqueloutro']
        contractions[u'àqueloutras'] = [u'a', u'aqueloutras'], \
                                       [u'a', u'aqueloutro']

        contractions[u'do'] = [u'de', u'o'], [u'de', u'o']
        contractions[u'da'] = [u'de', u'a'], [u'de', u'o']
        contractions[u'dos'] = [u'de', u'os'], [u'de', u'o']
        contractions[u'das'] = [u'de', u'as'], [u'de', u'o']
        contractions[u'desse'] = [u'de', u'esse'], [u'de', u'esse']
        contractions[u'dessa'] = [u'de', u'essa'], [u'de', u'esse']
        contractions[u'desses'] = [u'de', u'esses'], [u'de', u'esse']
        contractions[u'dessas'] = [u'de', u'essas'], [u'de', u'esse']
        contractions[u'disso'] = [u'de', u'isso'], [u'de', u'isso']
        contractions[u'deste'] = [u'de', u'este'], [u'de', u'este']
        contractions[u'desta'] = [u'de', u'esta'], [u'de', u'este']
        contractions[u'destes'] = [u'de', u'estes'], [u'de', u'este']
        contractions[u'destas'] = [u'de', u'estas'], [u'de', u'este']
        contractions[u'disto'] = [u'de', u'isto'], [u'de', u'isto']
        contractions[u'daquele'] = [u'de', u'aquele'], [u'de', u'aquele']
        contractions[u'daquela'] = [u'de', u'aquela'], [u'de', u'aquele']
        contractions[u'daqueles'] = [u'de', u'aqueles'], [u'de', u'aquele']
        contractions[u'daquelas'] = [u'de', u'aquelas'], [u'de', u'aquele']
        contractions[u'daquilo'] = [u'de', u'aquilo'], [u'de', u'aquilo']
        contractions[u'dele'] = [u'de', u'ele'], [u'de', u'ele']
        contractions[u'dela'] = [u'de', u'ela'], [u'de', u'ele']
        contractions[u'deles'] = [u'de', u'eles'], [u'de', u'ele']
        contractions[u'delas'] = [u'de', u'elas'], [u'de', u'ele']
        contractions[u'dum'] = [u'de', u'um'], [u'de', u'um']
        contractions[u'duma'] = [u'de', u'uma'], [u'de', u'um']
        contractions[u'duns'] = [u'de', u'uns'], [u'de', u'um']
        contractions[u'dumas'] = [u'de', u'umas'], [u'de', u'um']
        contractions[u'doutro'] = [u'de', u'outro'], [u'de', u'outro']
        contractions[u'doutra'] = [u'de', u'outra'], [u'de', u'outro']
        contractions[u'doutros'] = [u'de', u'outros'], [u'de', u'outro']
        contractions[u'doutras'] = [u'de', u'outras'], [u'de', u'outro']
        contractions[u'daqueloutro'] = [u'de', u'aqueloutro'], \
                                       [u'de', u'aqueloutro']
        contractions[u'daqueloutra'] = [u'de', u'aqueloutra'], \
                                       [u'de', u'aqueloutro']
        contractions[u'daqueloutros'] = [u'de', u'aqueloutros'], \
                                        [u'de', u'aqueloutro']
        contractions[u'daqueloutras'] = [u'de', u'aqueloutras'], \
                                        [u'de', u'aqueloutro']
        contractions[u'dessoutro'] = [u'de', u'essoutro'], [u'de', u'essoutro']
        contractions[u'dessoutra'] = [u'de', u'essoutra'], [u'de', u'essoutro']
        contractions[u'dessoutros'] = [u'de', u'essoutros'], \
                                      [u'de', u'essoutro']
        contractions[u'dessoutras'] = [u'de', u'essoutras'], \
                                      [u'de', u'essoutro']
        contractions[u'destoutro'] = [u'de', u'estoutro'], [u'de', u'estoutro']
        contractions[u'destoutra'] = [u'de', u'estoutra'], [u'de', u'estoutro']
        contractions[u'destoutros'] = [u'de', u'estoutros'], \
                                      [u'de', u'estoutro']
        contractions[u'destoutras'] = [u'de', u'estoutras'], \
                                      [u'de', u'estoutro']
        contractions[u'dalgum'] = [u'de', u'algum'], [u'de', u'algum']
        contractions[u'dalguma'] = [u'de', u'alguma'], [u'de', u'algum']
        contractions[u'dalguns'] = [u'de', u'alguns'], [u'de', u'algum']
        contractions[u'dalgumas'] = [u'de', u'algumas'], [u'de', u'algum']
        contractions[u'dalguém'] = [u'de', u'alguém'], [u'de', u'alguém']
        contractions[u'dalgo'] = [u'de', u'algo'], [u'de', u'algo']
        contractions[u'dacolá'] = [u'de', u'acolá'], [u'de', u'acolá']
        contractions[u'dalgures'] = [u'de', u'algures'], [u'de', u'algures']
        contractions[u'dalhures'] = [u'de', u'alhures'], [u'de', u'alhures']
        contractions[u'dali'] = [u'de', u'ali'], [u'de', u'ali']
        contractions[u'daqui'] = [u'de', u'aqui'], [u'de', u'aqui']
        contractions[u'dentre'] = [u'de', u'entre'], [u'de', u'entre']
        contractions[u'donde'] = [u'de', u'onde'], [u'de', u'onde']
        contractions[u'doutrem'] = [u'de', u'outrem'], [u'de', u'outrem']
        contractions[u'doutrora'] = [u'de', u'outrora'], [u'de', u'outrora']
        contractions[u"d'el-rei"] = [u'de', u'el-rei'], [u'de', u'el-rei']

        contractions[u'no'] = [u'em', u'o'], [u'em', u'o']
        contractions[u'na'] = [u'em', u'a'], [u'em', u'o']
        contractions[u'nos'] = [u'em', u'os'], [u'em', u'o']
        contractions[u'nas'] = [u'em', u'as'], [u'em', u'o']
        contractions[u'nesse'] = [u'em', u'esse'], [u'em', u'esse']
        contractions[u'nessa'] = [u'em', u'essa'], [u'em', u'esse']
        contractions[u'nesses'] = [u'em', u'esses'], [u'em', u'esse']
        contractions[u'nessas'] = [u'em', u'essas'], [u'em', u'esse']
        contractions[u'nisso'] = [u'em', u'isso'], [u'em', u'isso']
        contractions[u'neste'] = [u'em', u'este'], [u'em', u'este']
        contractions[u'nesta'] = [u'em', u'esta'], [u'em', u'este']
        contractions[u'nestes'] = [u'em', u'estes'], [u'em', u'este']
        contractions[u'nestas'] = [u'em', u'estas'], [u'em', u'este']
        contractions[u'nisto'] = [u'em', u'isto'], [u'em', u'isto']
        contractions[u'naquele'] = [u'em', u'aquele'], [u'em', u'aquele']
        contractions[u'naquela'] = [u'em', u'aquela'], [u'em', u'aquele']
        contractions[u'naqueles'] = [u'em', u'aqueles'], [u'em', u'aquele']
        contractions[u'naquelas'] = [u'em', u'aquelas'], [u'em', u'aquele']
        contractions[u'naquilo'] = [u'em', u'aquilo'], [u'em', u'aquilo']
        contractions[u'nele'] = [u'em', u'ele'], [u'em', u'ele']
        contractions[u'nela'] = [u'em', u'ela'], [u'em', u'ele']
        contractions[u'neles'] = [u'em', u'eles'], [u'em', u'ele']
        contractions[u'nelas'] = [u'em', u'elas'], [u'em', u'ele']
        contractions[u'num'] = [u'em', u'um'], [u'em', u'um']
        contractions[u'numa'] = [u'em', u'uma'], [u'em', u'um']
        contractions[u'nuns'] = [u'em', u'uns'], [u'em', u'um']
        contractions[u'numas'] = [u'em', u'umas'], [u'em', u'um']
        contractions[u'noutro'] = [u'em', u'outro'], [u'em', u'outro']
        contractions[u'noutra'] = [u'em', u'outra'], [u'em', u'outro']
        contractions[u'noutros'] = [u'em', u'outros'], [u'em', u'outro']
        contractions[u'noutras'] = [u'em', u'outras'], [u'em', u'outro']
        contractions[u'naqueloutro'] = [u'em', u'aqueloutro'], \
                                       [u'em', u'aqueloutro']
        contractions[u'naqueloutra'] = [u'em', u'aqueloutra'], \
                                       [u'em', u'aqueloutro']
        contractions[u'naqueloutros'] = [u'em', u'aqueloutros'], \
                                        [u'em', u'aqueloutro']
        contractions[u'naqueloutras'] = [u'em', u'aqueloutras'], \
                                        [u'em', u'aqueloutro']
        contractions[u'nessoutro'] = [u'em', u'essoutro'], [u'em', u'essoutro']
        contractions[u'nessoutra'] = [u'em', u'essoutra'], [u'em', u'essoutro']
        contractions[u'nessoutros'] = [u'em', u'essoutros'], \
                                      [u'em', u'essoutro']
        contractions[u'nessoutras'] = [u'em', u'essoutras'], \
                                      [u'em', u'essoutro']
        contractions[u'nestoutro'] = [u'em', u'estoutro'], [u'em', u'estoutro']
        contractions[u'nestoutra'] = [u'em', u'estoutra'], [u'em', u'estoutro']
        contractions[u'nestoutros'] = [u'em', u'estoutros'], \
                                      [u'em', u'estoutro']
        contractions[u'nestoutras'] = [u'em', u'estoutras'], \
                                      [u'em', u'estoutro']
        contractions[u'nalgum'] = [u'em', u'algum'], [u'em', u'algum']
        contractions[u'nalguma'] = [u'em', u'alguma'], [u'em', u'algum']
        contractions[u'nalguns'] = [u'em', u'alguns'], [u'em', u'algum']
        contractions[u'nalgumas'] = [u'em', u'algumas'], [u'em', u'algum']
        contractions[u'nalguém'] = [u'em', u'alguém'], [u'em', u'alguém']
        contractions[u'nalgo'] = [u'em', u'algo'], [u'em', u'algo']
        contractions[u'noutrem'] = [u'em', u'outrem'], [u'em', u'outrem']

        contractions[u'pelo'] = [u'por', u'o'], [u'por', u'o']
        contractions[u'pela'] = [u'por', u'a'], [u'por', u'o']
        contractions[u'pelos'] = [u'por', u'os'], [u'por', u'o']
        contractions[u'pelas'] = [u'por', u'as'], [u'por', u'o']

        contractions[u'hei-de'] = [u'hei', u'de'], [u'haver', u'de']
        contractions[u'há-de'] = [u'há', u'de'], [u'haver', u'de']
        contractions[u'hás-de'] = [u'hás', u'de'], [u'haver', u'de']

        # Add upper cases.
        contraction_toks = contractions.keys()
        for tok in contraction_toks:
            first_char = tok[0]
            if first_char.islower():
                # NOTE: tok.title() fails when some character is not ASCII
                # (e.g. Dalguém).
                upper_tok = tok[0].upper() + tok[1:]
            else:
                upper_tok = regex.sub(u'^à', u'À', tok)
            words, lemmas = contractions[tok]
            upper_words = []
            upper_lemmas = []
            for word, lemma in zip(words, lemmas):
                if len(upper_words) == 0:
                    upper_words.append(word.title())
                else:
                    upper_words.append(word[:])
                upper_lemmas.append(lemma[:])
            contractions[upper_tok] = upper_words, upper_lemmas

        return contractions

    def _generate_clitics(self):
        """
        Generate clitics (e.g. '-se') and verb suffixes (e.g. '-ei') for
        Portuguese.

        :rtype: a list of suffixes and a list of clitics.
        """
        import numpy as np

        suffixes = set()
        clitics = set()

        # Suffixos dos verbos em mesoclíticos.
        suffixes.add(u'-ei')
        suffixes.add(u'-ás')
        suffixes.add(u'-á')
        suffixes.add(u'-emos')
        suffixes.add(u'-eis')
        suffixes.add(u'-ão')
        suffixes.add(u'-ia')
        suffixes.add(u'-ias')
        suffixes.add(u'-íamos')
        suffixes.add(u'-íeis')
        suffixes.add(u'-iam')

        # Nota: com -lo, -la, -los, -las, há sempre um r,s,z que cai. (Eis lo -> Ei-lo)
        # Com -no, -na, -nos, -nas, quase nunca cai ("dizem-nos"), a não ser
        # quando o verbo termina em "mos" ("lavamo-nos").

        # Note: with it, it, it, them, them, there is always an r, s, z that falls.
        # With -no, -na, -nos, -as almost never falls ("tell us"), except
        # when the verb ends in "mos" ("lavamo-nos").
        
        clitics.add(u'-lo')
        clitics.add(u'-la')
        clitics.add(u'-los')
        clitics.add(u'-las')

        clitics.add(u'-na')
        clitics.add(u'-nas')
        clitics.add(u'-me')
        clitics.add(u'-te')
        clitics.add(u'-no')
        clitics.add(u'-nos')
        clitics.add(u'-vos')
        clitics.add(u'-o')
        clitics.add(u'-os')
        clitics.add(u'-a')
        clitics.add(u'-as')
        clitics.add(u'-se')
        clitics.add(u'-lhe')
        clitics.add(u'-lhes')
        clitics.add(u'-mo')
        clitics.add(u'-mos')
        clitics.add(u'-ma')
        clitics.add(u'-mas')
        clitics.add(u'-to')
        clitics.add(u'-tos')
        clitics.add(u'-ta')
        clitics.add(u'-tas')

        clitics.add(u'-no-lo')
        clitics.add(u'-no-los')
        clitics.add(u'-no-la')
        clitics.add(u'-no-las')
        clitics.add(u'-vo-lo')
        clitics.add(u'-vo-los')
        clitics.add(u'-vo-la')
        clitics.add(u'-vo-las')
        clitics.add(u'-lho')
        clitics.add(u'-lhos')
        clitics.add(u'-lha')
        clitics.add(u'-lhas')
        clitics.add(u'-se-me')
        clitics.add(u'-se-te')
        clitics.add(u'-se-nos')
        clitics.add(u'-se-vos')
        clitics.add(u'-se-lhe')
        clitics.add(u'-se-lhes')
        clitics.add(u'-se-mo')
        clitics.add(u'-se-mos')
        clitics.add(u'-se-ma')
        clitics.add(u'-se-mas')
        clitics.add(u'-se-to')
        clitics.add(u'-se-tos')
        clitics.add(u'-se-ta')
        clitics.add(u'-se-tas')

        clitics.add(u'-se-no-lo')
        clitics.add(u'-se-no-los')
        clitics.add(u'-se-no-la')
        clitics.add(u'-se-no-las')
        clitics.add(u'-se-vo-lo')
        clitics.add(u'-se-vo-los')
        clitics.add(u'-se-vo-la')
        clitics.add(u'-se-vo-las')
        clitics.add(u'-se-lho')
        clitics.add(u'-se-lhos')
        clitics.add(u'-se-lha')
        clitics.add(u'-se-lhas')

        # Sort lists by length so that, if one suffix/clitic is a suffix of
        # another, we always check the longest one first.
        clitic_list = list(clitics)
        clitic_inds = list(np.argsort([len(clitic) for clitic in clitic_list]))
        clitic_inds.reverse()

        suffix_list = list(suffixes)
        suffix_inds = list(np.argsort([len(suffix) for suffix in suffix_list]))
        suffix_inds.reverse()

        final_clitic_list = [clitic_list[i] for i in clitic_inds]
        final_suffix_list = [suffix_list[i] for i in suffix_inds]

        return final_clitic_list, final_suffix_list

    def split_if_contraction(self, word):
        original_word = word

        # Handle preposition+determiner contractions.
        if word in self.contractions:
            return ' '.join(self.contractions[word][0])

        # Before looking at clitic regexes, check if the word is in a blacklist.
        if word in self.non_contractions:
            return word

        # Handle clitics. The output is consistent with the
        # Portuguese CINTIL corpus (e.g.: "fá-lo-ei" ->  "fá#-CL-ei" + "-lo").
        new_tok = word
        suffix_tok = ''
        for suffix in self.clitic_suffixes:
            if word.endswith(suffix):
                suffix_tok = word[-len(suffix):]
                new_tok = word[:-len(suffix)]
                break
        clitic_tok = ''
        for clitic in self.clitics:
            if new_tok.endswith(clitic):
                clitic_tok = new_tok[-len(clitic):]
                new_tok = new_tok[:-len(clitic)]
                break
        if clitic_tok != '':
            if clitic_tok in [u'-lo', u'-la', u'-los', u'-las']:
                if self.mark_enclitics:
                    new_tok += u'#'
            if suffix_tok != '':
                if self.mark_enclitics:
                    new_tok += u'-CL' + suffix_tok
                elif suffix_tok.startswith(u'-'):
                    new_tok += suffix_tok[1:]
                else:
                    new_tok += suffix_tok
            word = ' '.join([new_tok, clitic_tok])

        return word

    def realise_surface_form(self, sentence):
        # Handle preposition+determiner contractions.
        sentence = regex.sub(r'(^|\s)a o($|\s)', r'\1ao\2', sentence)  # contractions[u'ao'] = [u'a', u'o']
        sentence = regex.sub(r'(^|\s)A o($|\s)', r'\1Ao\2', sentence)

        sentence = regex.sub(r'(^|\s)a a($|\s)', r'\1à\2', sentence)  # contractions[u'à'] = [u'a', u'a']
        sentence = regex.sub(r'(^|\s)A a($|\s)', r'\1À\2', sentence)

        sentence = regex.sub(r'(^|\s)a os($|\s)', r'\1aos\2', sentence)  # contractions[u'aos'] = [u'a', u'os']
        sentence = regex.sub(r'(^|\s)A os($|\s)', r'\1Aos\2', sentence)

        sentence = regex.sub(r'(^|\s)a os($|\s)', r'\1às\2', sentence)  # contractions[u'às'] = [u'a', u'as']
        sentence = regex.sub(r'(^|\s)A os($|\s)', r'\1Às\2', sentence)

        sentence = regex.sub(r'(^|\s)a aquele($|\s)', r'\1àquele\2', sentence)  # contractions[u'àquele'] = [u'a', u'aquele']
        sentence = regex.sub(r'(^|\s)A aquele($|\s)', r'\1Àquele\2', sentence)

        sentence = regex.sub(r'(^|\s)a aquela($|\s)', r'\1àquela\2', sentence)  # contractions[u'àquela'] = [u'a', u'aquela']
        sentence = regex.sub(r'(^|\s)A aquela($|\s)', r'\1Àquela\2', sentence)

        sentence = regex.sub(r'(^|\s)a aqueles($|\s)', r'\1àqueles\2', sentence)  # contractions[u'àqueles'] = [u'a', u'aqueles']
        sentence = regex.sub(r'(^|\s)A aqueles($|\s)', r'\1Àqueles\2', sentence)

        sentence = regex.sub(r'(^|\s)a aquelas($|\s)', r'\1àquelas\2', sentence)  # contractions[u'àquelas'] = [u'a', u'aquelas']
        sentence = regex.sub(r'(^|\s)A aquelas($|\s)', r'\1Àquelas\2', sentence)

        sentence = regex.sub(r'(^|\s)a aquilo($|\s)', r'\1àquilo\2', sentence)  # contractions[u'àquilo'] = [u'a', u'aquilo']
        sentence = regex.sub(r'(^|\s)A aquilo($|\s)', r'\1Àquilo\2', sentence)

        sentence = regex.sub(r'(^|\s)a onde($|\s)', r'\1aonde\2', sentence)  # contractions[u'aonde'] = [u'a', u'onde']
        sentence = regex.sub(r'(^|\s)A onde($|\s)', r'\1Aonde\2', sentence)

        sentence = regex.sub(r'(^|\s)a aqueloutro($|\s)', r'\1àqueloutro\2', sentence)  # contractions[u'àqueloutro'] = [u'a', u'aqueloutro']
        sentence = regex.sub(r'(^|\s)A aqueloutro($|\s)', r'\1Àqueloutro\2', sentence)

        sentence = regex.sub(r'(^|\s)a aqueloutra($|\s)', r'\1àqueloutra\2', sentence)  # contractions[u'àqueloutra'] = [u'a', u'aqueloutra']
        sentence = regex.sub(r'(^|\s)A aqueloutra($|\s)', r'\1Àqueloutra\2', sentence)

        sentence = regex.sub(r'(^|\s)a aqueloutros($|\s)', r'\1àqueloutros\2', sentence)  # contractions[u'àqueloutros'] = [u'a', u'aqueloutros']
        sentence = regex.sub(r'(^|\s)A aqueloutros($|\s)', r'\1Àqueloutros\2', sentence)

        sentence = regex.sub(r'(^|\s)a aqueloutras($|\s)', r'\1àqueloutras\2', sentence)  # contractions[u'àqueloutras'] = [u'a', u'aqueloutras']
        sentence = regex.sub(r'(^|\s)A aqueloutras($|\s)', r'\1Àqueloutras\2', sentence)

        sentence = regex.sub(r'(^|\s)de o($|\s)', r'\1do\2', sentence)  # contractions[u'do'] = [u'de', u'o']
        sentence = regex.sub(r'(^|\s)De o($|\s)', r'\1Do\2', sentence)

        sentence = regex.sub(r'(^|\s)de a($|\s)', r'\1da\2', sentence)  # contractions[u'da'] = [u'de', u'a']
        sentence = regex.sub(r'(^|\s)De a($|\s)', r'\1Da\2', sentence)

        sentence = regex.sub(r'(^|\s)de os($|\s)', r'\1dos\2', sentence)  # contractions[u'dos'] = [u'de', u'os']
        sentence = regex.sub(r'(^|\s)De os($|\s)', r'\1Dos\2', sentence)

        sentence = regex.sub(r'(^|\s)de as($|\s)', r'\1das\2', sentence)  # contractions[u'das'] = [u'de', u'as']
        sentence = regex.sub(r'(^|\s)De as($|\s)', r'\1Das\2', sentence)

        sentence = regex.sub(r'(^|\s)de esse($|\s)', r'\1desse\2', sentence)  # contractions[u'desse'] = [u'de', u'esse']
        sentence = regex.sub(r'(^|\s)De esse($|\s)', r'\1Desse\2', sentence)

        sentence = regex.sub(r'(^|\s)de essa($|\s)', r'\1dessa\2', sentence)  # contractions[u'dessa'] = [u'de', u'essa']
        sentence = regex.sub(r'(^|\s)De essa($|\s)', r'\1Dessa\2', sentence)

        sentence = regex.sub(r'(^|\s)de esses($|\s)', r'\1desses\2', sentence)  # contractions[u'desses'] = [u'de', u'esses']
        sentence = regex.sub(r'(^|\s)De esses($|\s)', r'\1Desses\2', sentence)

        sentence = regex.sub(r'(^|\s)de essas($|\s)', r'\1dessas\2', sentence)  # contractions[u'dessas'] = [u'de', u'essas']
        sentence = regex.sub(r'(^|\s)De essas($|\s)', r'\1Dessas\2', sentence)

        sentence = regex.sub(r'(^|\s)de isso($|\s)', r'\1disso\2', sentence)  # contractions[u'disso'] = [u'de', u'isso']
        sentence = regex.sub(r'(^|\s)De isso($|\s)', r'\1Disso\2', sentence)

        sentence = regex.sub(r'(^|\s)de este($|\s)', r'\1deste\2', sentence)  # contractions[u'deste'] = [u'de', u'este']
        sentence = regex.sub(r'(^|\s)De este($|\s)', r'\1Deste\2', sentence)

        sentence = regex.sub(r'(^|\s)de esta($|\s)', r'\1desta\2', sentence)  # contractions[u'desta'] = [u'de', u'esta']
        sentence = regex.sub(r'(^|\s)De esta($|\s)', r'\1Desta\2', sentence)

        sentence = regex.sub(r'(^|\s)de estes($|\s)', r'\1destes\2', sentence)  # contractions[u'destes'] = [u'de', u'estes']
        sentence = regex.sub(r'(^|\s)De estes($|\s)', r'\1Destes\2', sentence)

        sentence = regex.sub(r'(^|\s)de estas($|\s)', r'\1destas\2', sentence)  # contractions[u'destas'] = [u'de', u'estas']
        sentence = regex.sub(r'(^|\s)De estas($|\s)', r'\1Destas\2', sentence)

        sentence = regex.sub(r'(^|\s)de isto($|\s)', r'\1desto\2', sentence)  # contractions[u'disto'] = [u'de', u'isto']
        sentence = regex.sub(r'(^|\s)De isto($|\s)', r'\1Desto\2', sentence)

        sentence = regex.sub(r'(^|\s)de aquele($|\s)', r'\1daquele\2', sentence)  # contractions[u'daquele'] = [u'de', u'aquele']
        sentence = regex.sub(r'(^|\s)De aquele($|\s)', r'\1Daquele\2', sentence)

        sentence = regex.sub(r'(^|\s)de aquela($|\s)', r'\1daquela\2', sentence)  # contractions[u'daquela'] = [u'de', u'aquela']
        sentence = regex.sub(r'(^|\s)De aquela($|\s)', r'\1Daquela\2', sentence)

        sentence = regex.sub(r'(^|\s)de aqueles($|\s)', r'\1daqueles\2', sentence)  # contractions[u'daqueles'] = [u'de', u'aqueles']
        sentence = regex.sub(r'(^|\s)De aqueles($|\s)', r'\1Daqueles\2', sentence)

        sentence = regex.sub(r'(^|\s)de aquelas($|\s)', r'\1daquelas\2', sentence)  # contractions[u'daquelas'] = [u'de', u'aquelas']
        sentence = regex.sub(r'(^|\s)De aquelas($|\s)', r'\1Daquelas\2', sentence)

        sentence = regex.sub(r'(^|\s)de aquilo($|\s)', r'\1daquilo\2', sentence)  # contractions[u'daquilo'] = [u'de', u'aquilo']
        sentence = regex.sub(r'(^|\s)De aquilo($|\s)', r'\1Daquilo\2', sentence)

        sentence = regex.sub(r'(^|\s)de ele($|\s)', r'\1dele\2', sentence)  # contractions[u'dele'] = [u'de', u'ele']
        sentence = regex.sub(r'(^|\s)De ele($|\s)', r'\1Dele\2', sentence)

        sentence = regex.sub(r'(^|\s)de ela($|\s)', r'\1dela\2', sentence)  # contractions[u'dela'] = [u'de', u'ela']
        sentence = regex.sub(r'(^|\s)De ela($|\s)', r'\1Dela\2', sentence)

        sentence = regex.sub(r'(^|\s)de eles($|\s)', r'\1deles\2', sentence)  # contractions[u'deles'] = [u'de', u'eles']
        sentence = regex.sub(r'(^|\s)De eles($|\s)', r'\1Deles\2', sentence)

        sentence = regex.sub(r'(^|\s)de elas($|\s)', r'\1delas\2', sentence)  # contractions[u'delas'] = [u'de', u'elas']
        sentence = regex.sub(r'(^|\s)De elas($|\s)', r'\1Delas\2', sentence)

        sentence = regex.sub(r'(^|\s)de um($|\s)', r'\1dum\2', sentence)  # contractions[u'dum'] = [u'de', u'um']
        sentence = regex.sub(r'(^|\s)De um($|\s)', r'\1Dum\2', sentence)

        sentence = regex.sub(r'(^|\s)de uma($|\s)', r'\1duma\2', sentence)  # contractions[u'duma'] = [u'de', u'uma']
        sentence = regex.sub(r'(^|\s)De uma($|\s)', r'\1Duma\2', sentence)

        sentence = regex.sub(r'(^|\s)de uns($|\s)', r'\1duns\2', sentence)  # contractions[u'duns'] = [u'de', u'uns']
        sentence = regex.sub(r'(^|\s)De uns($|\s)', r'\1Duns\2', sentence)

        sentence = regex.sub(r'(^|\s)de umas($|\s)', r'\1dumas\2', sentence)  # contractions[u'dumas'] = [u'de', u'umas']
        sentence = regex.sub(r'(^|\s)De umas($|\s)', r'\1Dumas\2', sentence)

        sentence = regex.sub(r'(^|\s)de outro($|\s)', r'\1doutro\2', sentence)  # contractions[u'doutro'] = [u'de', u'outro']
        sentence = regex.sub(r'(^|\s)De outro($|\s)', r'\1Doutro\2', sentence)

        sentence = regex.sub(r'(^|\s)de outra($|\s)', r'\1doutra\2', sentence)  # contractions[u'doutra'] = [u'de', u'outra']
        sentence = regex.sub(r'(^|\s)De outra($|\s)', r'\1Doutra\2', sentence)

        sentence = regex.sub(r'(^|\s)de outros($|\s)', r'\1doutros\2', sentence)  # contractions[u'doutros'] = [u'de', u'outros']
        sentence = regex.sub(r'(^|\s)De outros($|\s)', r'\1Doutros\2', sentence)

        sentence = regex.sub(r'(^|\s)de outras($|\s)', r'\1doutras\2', sentence)  # contractions[u'doutras'] = [u'de', u'outras']
        sentence = regex.sub(r'(^|\s)De outras($|\s)', r'\1Doutras\2', sentence)

        sentence = regex.sub(r'(^|\s)de aqueloutro($|\s)', r'\1daqueloutro\2', sentence)  # contractions[u'daqueloutro'] = [u'de', u'aqueloutro']
        sentence = regex.sub(r'(^|\s)De aqueloutro($|\s)', r'\1Daqueloutro\2', sentence)

        sentence = regex.sub(r'(^|\s)de aqueloutra($|\s)', r'\1daqueloutra\2', sentence)  # contractions[u'daqueloutra'] = [u'de', u'aqueloutra']
        sentence = regex.sub(r'(^|\s)De aqueloutra($|\s)', r'\1Daqueloutra\2', sentence)

        sentence = regex.sub(r'(^|\s)de aqueloutros($|\s)', r'\1daqueloutros\2', sentence)  # contractions[u'daqueloutros'] = [u'de', u'aqueloutros']
        sentence = regex.sub(r'(^|\s)De aqueloutros($|\s)', r'\1Daqueloutros\2', sentence)

        sentence = regex.sub(r'(^|\s)de aqueloutras($|\s)', r'\1daqueloutras\2', sentence)  # contractions[u'daqueloutras'] = [u'de', u'aqueloutras']
        sentence = regex.sub(r'(^|\s)De aqueloutras($|\s)', r'\1Daqueloutras\2', sentence)

        sentence = regex.sub(r'(^|\s)de essoutro($|\s)', r'\1dessoutro\2', sentence)  # contractions[u'dessoutro'] = [u'de', u'essoutro']
        sentence = regex.sub(r'(^|\s)De essoutro($|\s)', r'\1Dessoutro\2', sentence)

        sentence = regex.sub(r'(^|\s)de essoutra($|\s)', r'\1dessoutra\2', sentence)  # contractions[u'dessoutra'] = [u'de', u'essoutra']
        sentence = regex.sub(r'(^|\s)De essoutra($|\s)', r'\1Dessoutra\2', sentence)

        sentence = regex.sub(r'(^|\s)de essoutros($|\s)', r'\1dessoutros\2', sentence)  # contractions[u'dessoutros'] = [u'de', u'essoutros']
        sentence = regex.sub(r'(^|\s)De essoutros($|\s)', r'\1Dessoutros\2', sentence)

        sentence = regex.sub(r'(^|\s)de essoutras($|\s)', r'\1dessoutras\2', sentence)  # contractions[u'dessoutras'] = [u'de', u'essoutras']
        sentence = regex.sub(r'(^|\s)De essoutras($|\s)', r'\1Dessoutras\2', sentence)

        sentence = regex.sub(r'(^|\s)de estoutro($|\s)', r'\1destoutro\2', sentence)  # contractions[u'destoutro'] = [u'de', u'estoutro']
        sentence = regex.sub(r'(^|\s)De estoutro($|\s)', r'\1Destoutro\2', sentence)

        sentence = regex.sub(r'(^|\s)de estoutra($|\s)', r'\1destoutra\2', sentence)  # contractions[u'destoutra'] = [u'de', u'estoutra']
        sentence = regex.sub(r'(^|\s)De estoutra($|\s)', r'\1Destoutra\2', sentence)

        sentence = regex.sub(r'(^|\s)de estoutros($|\s)', r'\1destoutros\2', sentence)  # contractions[u'destoutros'] = [u'de', u'estoutros']
        sentence = regex.sub(r'(^|\s)De estoutros($|\s)', r'\1Destoutros\2', sentence)

        sentence = regex.sub(r'(^|\s)de estoutras($|\s)', r'\1destoutras\2', sentence)  # contractions[u'destoutras'] = [u'de', u'estoutras']
        sentence = regex.sub(r'(^|\s)De estoutras($|\s)', r'\1Destoutras\2', sentence)

        sentence = regex.sub(r'(^|\s)de algum($|\s)', r'\1dalgum\2', sentence)  # contractions[u'dalgum'] = [u'de', u'algum']
        sentence = regex.sub(r'(^|\s)De algum($|\s)', r'\1Dalgum\2', sentence)

        sentence = regex.sub(r'(^|\s)de alguma($|\s)', r'\1dalguma\2', sentence)  # contractions[u'dalguma'] = [u'de', u'alguma']
        sentence = regex.sub(r'(^|\s)De alguma($|\s)', r'\1Dalguma\2', sentence)

        sentence = regex.sub(r'(^|\s)de alguns($|\s)', r'\1dalguns\2', sentence)  # contractions[u'dalguns'] = [u'de', u'alguns']
        sentence = regex.sub(r'(^|\s)De alguns($|\s)', r'\1Dalguns\2', sentence)

        sentence = regex.sub(r'(^|\s)de algumas($|\s)', r'\1dalgumas\2', sentence)  # contractions[u'dalgumas'] = [u'de', u'algumas']
        sentence = regex.sub(r'(^|\s)De algumas($|\s)', r'\1Dalgumas\2', sentence)

        sentence = regex.sub(r'(^|\s)de alguém($|\s)', r'\1dalguém\2', sentence)  # contractions[u'dalguém'] = [u'de', u'alguém']
        sentence = regex.sub(r'(^|\s)De alguém($|\s)', r'\1Dalguém\2', sentence)

        sentence = regex.sub(r'(^|\s)de algo($|\s)', r'\1dalgo\2', sentence)  # contractions[u'dalgo'] = [u'de', u'algo']
        sentence = regex.sub(r'(^|\s)De algo($|\s)', r'\1Dalgo\2', sentence)

        sentence = regex.sub(r'(^|\s)de acolá($|\s)', r'\1dacolá\2', sentence)  # contractions[u'dacolá'] = [u'de', u'acolá']
        sentence = regex.sub(r'(^|\s)De acolá($|\s)', r'\1Dacolá\2', sentence)

        sentence = regex.sub(r'(^|\s)de algures($|\s)', r'\1dalgures\2', sentence)  # contractions[u'dalgures'] = [u'de', u'algures']
        sentence = regex.sub(r'(^|\s)De algures($|\s)', r'\1Dalgures\2', sentence)

        sentence = regex.sub(r'(^|\s)de alhures($|\s)', r'\1dalhures\2', sentence)  # contractions[u'dalhures'] = [u'de', u'alhures']
        sentence = regex.sub(r'(^|\s)De alhures($|\s)', r'\1Dalhures\2', sentence)

        sentence = regex.sub(r'(^|\s)de ali($|\s)', r'\1dali\2', sentence)  # contractions[u'dali'] = [u'de', u'ali']
        sentence = regex.sub(r'(^|\s)De ali($|\s)', r'\1Dali\2', sentence)

        sentence = regex.sub(r'(^|\s)de aqui($|\s)', r'\1daqui\2', sentence)  # contractions[u'daqui'] = [u'de', u'aqui']
        sentence = regex.sub(r'(^|\s)De aqui($|\s)', r'\1Daqui\2', sentence)

        sentence = regex.sub(r'(^|\s)de entre($|\s)', r'\1dentre\2', sentence)  # contractions[u'dentre'] = [u'de', u'entre']
        sentence = regex.sub(r'(^|\s)De entre($|\s)', r'\1Dentre\2', sentence)

        sentence = regex.sub(r'(^|\s)de onde($|\s)', r'\1donde\2', sentence)  # contractions[u'donde'] = [u'de', u'onde']
        sentence = regex.sub(r'(^|\s)De onde($|\s)', r'\1Donde\2', sentence)

        sentence = regex.sub(r'(^|\s)de outrem($|\s)', r'\1doutrem\2', sentence)  # contractions[u'doutrem'] = [u'de', u'outrem']
        sentence = regex.sub(r'(^|\s)De outrem($|\s)', r'\1Doutrem\2', sentence)

        sentence = regex.sub(r'(^|\s)de outrora($|\s)', r'\1doutrora\2', sentence)  # contractions[u'doutrora'] = [u'de', u'outrora']
        sentence = regex.sub(r'(^|\s)De outrora($|\s)', r'\1Doutrora\2', sentence)

        sentence = regex.sub(r'(^|\s)de el-rei($|\s)', r'\1d\'el-rei\2', sentence)  # contractions[u"d'el-rei"] = [u'de', u'el-rei']
        sentence = regex.sub(r'(^|\s)De el-rei($|\s)', r'\1D\'el-rei\2', sentence)

        sentence = regex.sub(r'(^|\s)em o($|\s)', r'\1no\2', sentence)  # contractions[u'no'] = [u'em', u'o']
        sentence = regex.sub(r'(^|\s)Em o($|\s)', r'\1No\2', sentence)

        sentence = regex.sub(r'(^|\s)em a($|\s)', r'\1na\2', sentence)  # contractions[u'na'] = [u'em', u'a']
        sentence = regex.sub(r'(^|\s)Em a($|\s)', r'\1Na\2', sentence)

        sentence = regex.sub(r'(^|\s)em os($|\s)', r'\1nos\2', sentence)  # contractions[u'nos'] = [u'em', u'os']
        sentence = regex.sub(r'(^|\s)Em os($|\s)', r'\1Nos\2', sentence)

        sentence = regex.sub(r'(^|\s)em as($|\s)', r'\1nas\2', sentence)  # contractions[u'nas'] = [u'em', u'as']
        sentence = regex.sub(r'(^|\s)Em as($|\s)', r'\1Nas\2', sentence)

        sentence = regex.sub(r'(^|\s)em esse($|\s)', r'\1nesse\2', sentence)  # contractions[u'nesse'] = [u'em', u'esse']
        sentence = regex.sub(r'(^|\s)Em esse($|\s)', r'\1Nesse\2', sentence)

        sentence = regex.sub(r'(^|\s)em essa($|\s)', r'\1nessa\2', sentence)  # contractions[u'nessa'] = [u'em', u'essa']
        sentence = regex.sub(r'(^|\s)Em essa($|\s)', r'\1Nessa\2', sentence)

        sentence = regex.sub(r'(^|\s)em esses($|\s)', r'\1nesses\2', sentence)  # contractions[u'nesses'] = [u'em', u'esses']
        sentence = regex.sub(r'(^|\s)Em esses($|\s)', r'\1Nesses\2', sentence)

        sentence = regex.sub(r'(^|\s)em essas($|\s)', r'\1nessas\2', sentence)  # contractions[u'nessas'] = [u'em', u'essas']
        sentence = regex.sub(r'(^|\s)Em essas($|\s)', r'\1Nessas\2', sentence)

        sentence = regex.sub(r'(^|\s)em isso($|\s)', r'\1nisso\2', sentence)  # contractions[u'nisso'] = [u'em', u'isso']
        sentence = regex.sub(r'(^|\s)Em isso($|\s)', r'\1Nisso\2', sentence)

        sentence = regex.sub(r'(^|\s)em este($|\s)', r'\1neste\2', sentence)  # contractions[u'neste'] = [u'em', u'este']
        sentence = regex.sub(r'(^|\s)Em este($|\s)', r'\1Neste\2', sentence)

        sentence = regex.sub(r'(^|\s)em esta($|\s)', r'\1nesta\2', sentence)  # contractions[u'nesta'] = [u'em', u'esta']
        sentence = regex.sub(r'(^|\s)Em esta($|\s)', r'\1Nesta\2', sentence)

        sentence = regex.sub(r'(^|\s)em estes($|\s)', r'\1nestes\2', sentence)  # contractions[u'nestes'] = [u'em', u'estes']
        sentence = regex.sub(r'(^|\s)Em estes($|\s)', r'\1Nestes\2', sentence)

        sentence = regex.sub(r'(^|\s)em estas($|\s)', r'\1nestas\2', sentence)  # contractions[u'nestas'] = [u'em', u'estas']
        sentence = regex.sub(r'(^|\s)Em estas($|\s)', r'\1Nestas\2', sentence)

        sentence = regex.sub(r'(^|\s)em isto($|\s)', r'\1nisto\2', sentence)  # contractions[u'nisto'] = [u'em', u'isto']
        sentence = regex.sub(r'(^|\s)Em isto($|\s)', r'\1Nisto\2', sentence)

        sentence = regex.sub(r'(^|\s)em aquele($|\s)', r'\1naquele\2', sentence)  # contractions[u'naquele'] = [u'em', u'aquele']
        sentence = regex.sub(r'(^|\s)Em aquele($|\s)', r'\1Naquele\2', sentence)

        sentence = regex.sub(r'(^|\s)em aquela($|\s)', r'\1naquela\2', sentence)  # contractions[u'naquela'] = [u'em', u'aquela']
        sentence = regex.sub(r'(^|\s)Em aquela($|\s)', r'\1Naquela\2', sentence)

        sentence = regex.sub(r'(^|\s)em aqueles($|\s)', r'\1naqueles\2', sentence)  # contractions[u'naqueles'] = [u'em', u'aqueles']
        sentence = regex.sub(r'(^|\s)Em aqueles($|\s)', r'\1Naqueles\2', sentence)

        sentence = regex.sub(r'(^|\s)em aquelas($|\s)', r'\1naquelas\2', sentence)  # contractions[u'naquelas'] = [u'em', u'aquelas']
        sentence = regex.sub(r'(^|\s)Em aquelas($|\s)', r'\1Naquelas\2', sentence)

        sentence = regex.sub(r'(^|\s)em aquilo($|\s)', r'\1naquilo\2', sentence)  # contractions[u'naquilo'] = [u'em', u'aquilo']
        sentence = regex.sub(r'(^|\s)Em aquilo($|\s)', r'\1Naquilo\2', sentence)

        sentence = regex.sub(r'(^|\s)em ele($|\s)', r'\1nele\2', sentence)  # contractions[u'nele'] = [u'em', u'ele']
        sentence = regex.sub(r'(^|\s)Em ele($|\s)', r'\1Nele\2', sentence)

        sentence = regex.sub(r'(^|\s)em ela($|\s)', r'\1nela\2', sentence)  # contractions[u'nela'] = [u'em', u'ela']
        sentence = regex.sub(r'(^|\s)Em ela($|\s)', r'\1Nela\2', sentence)

        sentence = regex.sub(r'(^|\s)em eles($|\s)', r'\1neles\2', sentence)  # contractions[u'neles'] = [u'em', u'eles']
        sentence = regex.sub(r'(^|\s)Em eles($|\s)', r'\1Neles\2', sentence)

        sentence = regex.sub(r'(^|\s)em elas($|\s)', r'\1nelas\2', sentence)  # contractions[u'nelas'] = [u'em', u'elas']
        sentence = regex.sub(r'(^|\s)Em elas($|\s)', r'\1Nelas\2', sentence)

        sentence = regex.sub(r'(^|\s)em um($|\s)', r'\1num\2', sentence)  # contractions[u'num'] = [u'em', u'um']
        sentence = regex.sub(r'(^|\s)Em um($|\s)', r'\1Num\2', sentence)

        sentence = regex.sub(r'(^|\s)em uma($|\s)', r'\1numa\2', sentence)  # contractions[u'numa'] = [u'em', u'uma']
        sentence = regex.sub(r'(^|\s)Em uma($|\s)', r'\1Numa\2', sentence)

        sentence = regex.sub(r'(^|\s)em uns($|\s)', r'\1nuns\2', sentence)  # contractions[u'nuns'] = [u'em', u'uns']
        sentence = regex.sub(r'(^|\s)Em uns($|\s)', r'\1Nuns\2', sentence)

        sentence = regex.sub(r'(^|\s)em umas($|\s)', r'\1numas\2', sentence)  # contractions[u'numas'] = [u'em', u'umas']
        sentence = regex.sub(r'(^|\s)Em umas($|\s)', r'\1Numas\2', sentence)

        sentence = regex.sub(r'(^|\s)em outro($|\s)', r'\1noutro\2', sentence)  # contractions[u'noutro'] = [u'em', u'outro']
        sentence = regex.sub(r'(^|\s)Em outro($|\s)', r'\1Noutro\2', sentence)

        sentence = regex.sub(r'(^|\s)em outra($|\s)', r'\1noutra\2', sentence)  # contractions[u'noutra'] = [u'em', u'outra']
        sentence = regex.sub(r'(^|\s)Em outra($|\s)', r'\1Noutra\2', sentence)

        sentence = regex.sub(r'(^|\s)em outros($|\s)', r'\1noutros\2', sentence)  # contractions[u'noutros'] = [u'em', u'outros']
        sentence = regex.sub(r'(^|\s)Em outros($|\s)', r'\1Noutros\2', sentence)

        sentence = regex.sub(r'(^|\s)em outras($|\s)', r'\1noutras\2', sentence)  # contractions[u'noutras'] = [u'em', u'outras']
        sentence = regex.sub(r'(^|\s)Em outras($|\s)', r'\1Noutras\2', sentence)

        sentence = regex.sub(r'(^|\s)em aqueloutro($|\s)', r'\1naqueloutro\2', sentence)  # contractions[u'naqueloutro'] = [u'em', u'aqueloutro']
        sentence = regex.sub(r'(^|\s)Em aqueloutro($|\s)', r'\1Naqueloutro\2', sentence)

        sentence = regex.sub(r'(^|\s)em aqueloutra($|\s)', r'\1naqueloutra\2', sentence)  # contractions[u'naqueloutra'] = [u'em', u'aqueloutra']
        sentence = regex.sub(r'(^|\s)Em aqueloutra($|\s)', r'\1Naqueloutra\2', sentence)

        sentence = regex.sub(r'(^|\s)em aqueloutros($|\s)', r'\1naqueloutros\2', sentence)  # contractions[u'naqueloutros'] = [u'em', u'aqueloutros']
        sentence = regex.sub(r'(^|\s)Em aqueloutros($|\s)', r'\1Naqueloutros\2', sentence)

        sentence = regex.sub(r'(^|\s)em aqueloutras($|\s)', r'\1naqueloutras\2', sentence)  # contractions[u'naqueloutras'] = [u'em', u'aqueloutras']
        sentence = regex.sub(r'(^|\s)Em aqueloutras($|\s)', r'\1Naqueloutras\2', sentence)

        sentence = regex.sub(r'(^|\s)em essoutro($|\s)', r'\1nessoutro\2', sentence)  # contractions[u'nessoutro'] = [u'em', u'essoutro']
        sentence = regex.sub(r'(^|\s)Em essoutro($|\s)', r'\1Nessoutro\2', sentence)

        sentence = regex.sub(r'(^|\s)em essoutra($|\s)', r'\1nessoutra\2', sentence)  # contractions[u'nessoutra'] = [u'em', u'essoutra']
        sentence = regex.sub(r'(^|\s)Em essoutra($|\s)', r'\1Nessoutra\2', sentence)

        sentence = regex.sub(r'(^|\s)em essoutros($|\s)', r'\1nessoutros\2', sentence)  # contractions[u'nessoutros'] = [u'em', u'essoutros']
        sentence = regex.sub(r'(^|\s)Em essoutros($|\s)', r'\1Nessoutros\2', sentence)

        sentence = regex.sub(r'(^|\s)em essoutras($|\s)', r'\1nessoutras\2', sentence)  # ccontractions[u'nessoutras'] = [u'em', u'essoutras']
        sentence = regex.sub(r'(^|\s)Em essoutras($|\s)', r'\1Nessoutras\2', sentence)

        sentence = regex.sub(r'(^|\s)em estoutro($|\s)', r'\1nestoutro\2', sentence)  # contractions[u'nestoutro'] = [u'em', u'estoutro']
        sentence = regex.sub(r'(^|\s)Em estoutro($|\s)', r'\1Nestoutro\2', sentence)

        sentence = regex.sub(r'(^|\s)em estoutra($|\s)', r'\1nestoutra\2', sentence)  # contractions[u'nestoutra'] = [u'em', u'estoutra']
        sentence = regex.sub(r'(^|\s)Em estoutra($|\s)', r'\1Nestoutra\2', sentence)

        sentence = regex.sub(r'(^|\s)em estoutros($|\s)', r'\1nestoutros\2', sentence)  # contractions[u'nestoutros'] = [u'em', u'estoutros']
        sentence = regex.sub(r'(^|\s)Em estoutros($|\s)', r'\1Nestoutros\2', sentence)

        sentence = regex.sub(r'(^|\s)em estoutras($|\s)', r'\1nestoutras\2', sentence)  # contractions[u'nestoutras'] = [u'em', u'estoutras']
        sentence = regex.sub(r'(^|\s)Em estoutras($|\s)', r'\1Nestoutras\2', sentence)

        sentence = regex.sub(r'(^|\s)em algum($|\s)', r'\1nalgum\2', sentence)  # contractions[u'nalgum'] = [u'em', u'algum']
        sentence = regex.sub(r'(^|\s)Em algum($|\s)', r'\1Nalgum\2', sentence)

        sentence = regex.sub(r'(^|\s)em alguma($|\s)', r'\1nalguma\2', sentence)  # contractions[u'nalguma'] = [u'em', u'alguma']
        sentence = regex.sub(r'(^|\s)Em alguma($|\s)', r'\1Nalguma\2', sentence)

        sentence = regex.sub(r'(^|\s)em alguns($|\s)', r'\1nalguns\2', sentence)  # contractions[u'nalguns'] = [u'em', u'alguns']
        sentence = regex.sub(r'(^|\s)Em alguns($|\s)', r'\1Nalguns\2', sentence)

        sentence = regex.sub(r'(^|\s)em algumas($|\s)', r'\1nalgumas\2', sentence)  # contractions[u'nalgumas'] = [u'em', u'algumas']
        sentence = regex.sub(r'(^|\s)Em algumas($|\s)', r'\1Nalgumas\2', sentence)

        sentence = regex.sub(r'(^|\s)em alguém($|\s)', r'\1nalguém\2', sentence)  # contractions[u'nalguém'] = [u'em', u'alguém']
        sentence = regex.sub(r'(^|\s)Em alguém($|\s)', r'\1Nalguém\2', sentence)

        sentence = regex.sub(r'(^|\s)em algo($|\s)', r'\1nalgo\2', sentence)  # contractions[u'nalgo'] = [u'em', u'algo']
        sentence = regex.sub(r'(^|\s)Em algo($|\s)', r'\1Nalgo\2', sentence)

        sentence = regex.sub(r'(^|\s)em outrem($|\s)', r'\1noutrem\2', sentence)  # contractions[u'noutrem'] = [u'em', u'outrem']
        sentence = regex.sub(r'(^|\s)Em outrem($|\s)', r'\1Noutrem\2', sentence)

        sentence = regex.sub(r'(^|\s)por o($|\s)', r'\1pelo\2', sentence)  # contractions[u'pelo'] = [u'por', u'o']
        sentence = regex.sub(r'(^|\s)Por o($|\s)', r'\1Pelo\2', sentence)

        sentence = regex.sub(r'(^|\s)por a($|\s)', r'\1pela\2', sentence)  # contractions[u'pela'] = [u'por', u'a']
        sentence = regex.sub(r'(^|\s)Por a($|\s)', r'\1Pela\2', sentence)

        sentence = regex.sub(r'(^|\s)por os($|\s)', r'\1pelos\2', sentence)  # contractions[u'pelos'] = [u'por', u'os']
        sentence = regex.sub(r'(^|\s)Por os($|\s)', r'\1Pelos\2', sentence)

        sentence = regex.sub(r'(^|\s)por as($|\s)', r'\1pelas\2', sentence)  # contractions[u'pelas'] = [u'por', u'as']
        sentence = regex.sub(r'(^|\s)Por as($|\s)', r'\1Pelas\2', sentence)

        sentence = regex.sub(r'(^|\s)hei de($|\s)', r'\1hei-de\2', sentence)  # contractions[u'hei-de'] = [u'hei', u'de']
        sentence = regex.sub(r'(^|\s)Hei de($|\s)', r'\1Hei-de\2', sentence)

        sentence = regex.sub(r'(^|\s)há de($|\s)', r'\1há-de\2', sentence)  # contractions[u'há-de'] = [u'há', u'de']
        sentence = regex.sub(r'(^|\s)Há de($|\s)', r'\1Há-de\2', sentence)

        sentence = regex.sub(r'(^|\s)hás de($|\s)', r'\1hás-de\2', sentence)  # contractions[u'hás-de'] = [u'hás', u'de']
        sentence = regex.sub(r'(^|\s)Hás de($|\s)', r'\1Hás-de\2', sentence)


        #### Clitics ####
        # sentence = regex.sub(r' lo($|\s)', r'-lo\1', sentence)
        # sentence = regex.sub(r' la($|\s)', r'-la\1', sentence)
        # sentence = regex.sub(r' los($|\s)', r'-los\1', sentence)
        # sentence = regex.sub(r' las($|\s)', r'-las\1', sentence)
        # clitics.add(u'-lo')
        # clitics.add(u'-la')
        # clitics.add(u'-los')
        # clitics.add(u'-las')



        return sentence
