# -*- coding: utf-8 -*-
import regex
from turbo_parser_tokeniser.contractions import Contractions


class GermanContractions(Contractions):
    def __init__(self):
        pass

    def split_if_contraction(self, word):
        # From the CONLLu corpus
        word = regex.sub(u'^(A|a)m$', u'\1n dem', word)
        word = regex.sub(u'^(A|a)ns$', u'\1n das', word)
        word = regex.sub(u'^(A|a)ufs$', u'\1uf das', word)
        word = regex.sub(u'^(B|b)eim$', u'\1ei dem', word)
        word = regex.sub(u'^(D|d)urchs$', u'\1urch das', word)  # Not found in the CONLLu corpus.
        word = regex.sub(u'^(F|f)ürs$', u'\1ür das', word)
        word = regex.sub(u'^(H|h)interm$', u'\1inter dem', word)  # Not found in the CONLLu corpus.
        word = regex.sub(u'^(I|i)m$', u'\1n dem', word)
        word = regex.sub(u'^(I|i)ns$', u'\1n das', word)
        word = regex.sub(u'^(Ü|ü)bers$', u'\1ber das', word)
        word = regex.sub(u'^(U|u)ms$', u'\1m das', word)
        word = regex.sub(u'^(U|u)nters$', u'\1nter das', word)  # Not found in the CONLLu corpus.
        word = regex.sub(u'^(U|u)nterm$', u'\1nter dem', word)  # Not found in the CONLLu corpus.
        word = regex.sub(u'^(V|v)om$', u'\1on dem', word)
        word = regex.sub(u'^(V|v)ors$', u'\1or das', word)  # Not found in the CONLLu corpus.
        word = regex.sub(u'^(V|v)orm$', u'\1or dem', word)  # Not found in the CONLLu corpus.
        word = regex.sub(u'^(Z|z)um$', u'\1u dem', word)
        word = regex.sub(u'^(Z|z)ur$', u'\1u der', word)

        return word
