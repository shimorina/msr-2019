# -*- coding: utf-8 -*-
from __future__ import print_function

import sys
from turbo_parser_tokeniser.spanish_contractions import SpanishContractions, SpanishAncoraContractions
from turbo_parser_tokeniser.italian_contractions import ItalianContractions
from turbo_parser_tokeniser.french_contractions import FrenchContractions
from turbo_parser_tokeniser.portuguese_contractions import PortugueseContractions
from turbo_parser_tokeniser.german_contractions import GermanContractions
from turbo_parser_tokeniser.english_contractions import EnglishContractions
from turbo_parser_tokeniser.arabic_contractions import ArabicContractions


class UniversalContractions(object):
    def __init__(self, language):
        if language == 'es':
            contraction_splitter = SpanishContractions()
        elif language == 'es-ancora':
            contraction_splitter = SpanishAncoraContractions()
        elif language == 'it':
            contraction_splitter = ItalianContractions()
        elif language == 'fr':
            contraction_splitter = FrenchContractions()
        elif language == 'pt':
            contraction_splitter = PortugueseContractions()
        elif language == 'de':
            contraction_splitter = GermanContractions()
        elif language == 'en':
            contraction_splitter = EnglishContractions()
        elif language == 'ar':
            contraction_splitter = ArabicContractions()
        else:
            print('No contraction splitter for language %s.' % language,
                  file=sys.stderr)
            contraction_splitter = None
        self.language = language
        self.contraction_splitter = contraction_splitter

    def split_if_contraction(self, word):
        if self.contraction_splitter is None:
            return word
        else:
            return self.contraction_splitter.split_if_contraction(word)

    def realise_surface_form(self, sentence):
        if self.contraction_splitter is None:
            return sentence
        else:
            return self.contraction_splitter.realise_surface_form(sentence)
