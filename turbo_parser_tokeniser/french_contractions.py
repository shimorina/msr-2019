# -*- coding: utf-8 -*-

import regex
from turbo_parser_tokeniser.contractions import Contractions


class FrenchContractions(Contractions):
    def __init__(self):
        pass

    def split_if_contraction(self, word):
        # Handle preposition+determiner contractions.
        word = regex.sub(u'^(A|a)u$', u'à le', word)
        word = regex.sub(u'^(A|a)uquel$', u'à lequel', word)
        word = regex.sub(u'^(A|a)ux$', u'à les', word)
        word = regex.sub(u'^(A|a)uxquels$', u'à lesquels', word)
        word = regex.sub(u'^(A|a)uxquelles$', u'à lesquelles', word)

        word = regex.sub(u'^(D|d)u$', u'de le', word)
        word = regex.sub(u'^(D|d)uquel$', u'de lequel', word)
        word = regex.sub(u'^(D|d)es$', u'de les', word)
        word = regex.sub(u'^(D|d)esquels$', u'de lesquels', word)
        word = regex.sub(u'^(D|d)esquelles$', u'de lesquelles', word)

        return word

    def realise_surface_form(self, sentence):
        # Handle preposition+determiner contractions.

        # also audit/audite, dudit/dudite from ledit??: not found in UD

        '''sentence = regex.sub(r'(^|\s)que ils($|\s)', r'\1qu\'ils\2', sentence)
        sentence = regex.sub(r'(^|\s)Que ils($|\s)', r'\1Qu\'ils\2', sentence)

        sentence = regex.sub(r'(^|\s)que il($|\s)', r'\1qu\'il\2', sentence)
        sentence = regex.sub(r'(^|\s)Que il($|\s)', r'\1Qu\'il\2', sentence)

        sentence = regex.sub(r'(^|\s)que elles($|\s)', r'\1qu\'elles\2', sentence)
        sentence = regex.sub(r'(^|\s)Que elles($|\s)', r'\1Qu\'elles\2', sentence)

        sentence = regex.sub(r'(^|\s)que elle($|\s)', r'\1qu\'elle\2', sentence)
        sentence = regex.sub(r'(^|\s)Que elle($|\s)', r'\1Qu\'elle\2', sentence)

        sentence = regex.sub(r'(^|\s)que on($|\s)', r'\1qu\'on\2', sentence)
        sentence = regex.sub(r'(^|\s)Que on($|\s)', r'\1Qu\'on\2', sentence)

        sentence = regex.sub(r'(^|\s)que on($|\s)', r'\1qu\'on\2', sentence)
        sentence = regex.sub(r'(^|\s)Que on($|\s)', r'\1Qu\'on\2', sentence)

        sentence = regex.sub(r'(^|\s)que est($|\s)', r'\1qu\'est\2', sentence)
        sentence = regex.sub(r'(^|\s)Que est($|\s)', r'\1Qu\'est\2', sentence)

        sentence = regex.sub(r'(^|\s)ce est($|\s)', r'\1c\'est\2', sentence)
        sentence = regex.sub(r'(^|\s)Ce est($|\s)', r'\1C\'est\2', sentence)'''

        sentence = regex.sub(r'(^|\s)ce (a|i|o|u|e|y|é|è|à|û|ô|î|â|ê|œ|A|I|O|U|E|Y|É|È|À|Û|Ô|Î|Â|Ê|Œ)', r"\1c' \2", sentence)
        sentence = regex.sub(r'(^|\s)Ce (a|i|o|u|e|y|é|è|à|û|ô|î|â|ê|œ|A|I|O|U|E|Y|É|È|À|Û|Ô|Î|Â|Ê|Œ)', r"\1C' \2", sentence)

        sentence = regex.sub(r'(^|\s)que (a|i|o|u|e|y|é|è|à|û|ô|î|â|ê|œ|A|I|O|U|E|Y|É|È|À|Û|Ô|Î|Â|Ê|Œ)', r"\1qu' \2", sentence)
        sentence = regex.sub(r'(^|\s)Que (a|i|o|u|e|y|é|è|à|û|ô|î|â|ê|œ|A|I|O|U|E|Y|É|È|À|Û|Ô|Î|Â|Ê|Œ)', r"\1Qu' \2", sentence)

        sentence = regex.sub(r'(^|\s)jusque (a|i|o|u|e|y|é|è|à|û|ô|î|â|ê|œ|A|I|O|U|E|Y|É|È|À|Û|Ô|Î|Â|Ê|Œ)', r"\1jusqu' \2", sentence)
        sentence = regex.sub(r'(^|\s)Jusque (a|i|o|u|e|y|é|è|à|û|ô|î|â|ê|œ|A|I|O|U|E|Y|É|È|À|Û|Ô|Î|Â|Ê|Œ)', r"\1Jusqu' \2", sentence)

        sentence = regex.sub(r'(^|\s)lorsque (a|i|o|u|e|y|é|è|à|û|ô|î|â|ê|œ|A|I|O|U|E|Y|É|È|À|Û|Ô|Î|Â|Ê|Œ)', r"\1lorsqu' \2", sentence)
        sentence = regex.sub(r'(^|\s)Lorsque (a|i|o|u|e|y|é|è|à|û|ô|î|â|ê|œ|A|I|O|U|E|Y|É|È|À|Û|Ô|Î|Â|Ê|Œ)', r"\1Lorsqu' \2", sentence)

        sentence = regex.sub(r'(^|\s)ne (a|i|o|u|e|y|é|è|à|û|ô|î|â|ê|œ|A|I|O|U|E|Y|É|È|À|Û|Ô|Î|Â|Ê|Œ)', r"\1n' \2", sentence)
        sentence = regex.sub(r'(^|\s)Ne (a|i|o|u|e|y|é|è|à|û|ô|î|â|ê|œ|A|I|O|U|E|Y|É|È|À|Û|Ô|Î|Â|Ê|Œ)', r"\1N' \2", sentence)

        sentence = regex.sub(r'(^|\s)me (a|i|o|u|e|y|é|è|à|û|ô|î|â|ê|œ|A|I|O|U|E|Y|É|È|À|Û|Ô|Î|Â|Ê|Œ)', r"\1m' \2", sentence)
        sentence = regex.sub(r'(^|\s)Me (a|i|o|u|e|y|é|è|à|û|ô|î|â|ê|œ|A|I|O|U|E|Y|É|È|À|Û|Ô|Î|Â|Ê|Œ)', r"\1M' \2", sentence)

        sentence = regex.sub(r'(^|\s)te (a|i|o|u|e|y|é|è|à|û|ô|î|â|ê|œ|A|I|O|U|E|Y|É|È|À|Û|Ô|Î|Â|Ê|Œ)', r"\1t' \2", sentence)
        sentence = regex.sub(r'(^|\s)Te (a|i|o|u|e|y|é|è|à|û|ô|î|â|ê|œ|A|I|O|U|E|Y|É|È|À|Û|Ô|Î|Â|Ê|Œ)', r"\1T' \2", sentence)

        sentence = regex.sub(r'(^|\s)se (a|i|o|u|e|y|é|è|à|û|ô|î|â|ê|œ|A|I|O|U|E|Y|É|È|À|Û|Ô|Î|Â|Ê|Œ)', r"\1s' \2", sentence)
        sentence = regex.sub(r'(^|\s)Se (a|i|o|u|e|y|é|è|à|û|ô|î|â|ê|œ|A|I|O|U|E|Y|É|È|À|Û|Ô|Î|Â|Ê|Œ)', r"\1S' \2", sentence)

        sentence = regex.sub(r'(^|\s)le (a|i|o|u|e|y|é|è|à|û|ô|î|â|ê|œ|A|I|O|U|E|Y|É|È|À|Û|Ô|Î|Â|Ê|Œ)', r"\1l' \2", sentence)
        sentence = regex.sub(r'(^|\s)Le (a|i|o|u|e|y|é|è|à|û|ô|î|â|ê|œ|A|I|O|U|E|Y|É|È|À|Û|Ô|Î|Â|Ê|Œ)', r"\1L' \2", sentence)

        sentence = regex.sub(r'(^|\s)la (a|i|o|u|e|y|é|è|à|û|ô|î|â|ê|œ|A|I|O|U|E|Y|É|È|À|Û|Ô|Î|Â|Ê|Œ)', r"\1l' \2", sentence)
        sentence = regex.sub(r'(^|\s)La (a|i|o|u|e|y|é|è|à|û|ô|î|â|ê|œ|A|I|O|U|E|Y|É|È|À|Û|Ô|Î|Â|Ê|Œ)', r"\1L' \2", sentence)

        sentence = regex.sub(r'(^|\s)de (a|i|o|u|e|y|é|è|à|û|ô|î|â|ê|œ|A|I|O|U|E|Y|É|È|À|Û|Ô|Î|Â|Ê|Œ)', r"\1d' \2", sentence)
        sentence = regex.sub(r'(^|\s)De (a|i|o|u|e|y|é|è|à|û|ô|î|â|ê|œ|A|I|O|U|E|Y|É|È|À|Û|Ô|Î|Â|Ê|Œ)', r"\1D' \2", sentence)

        sentence = regex.sub(r'(^|\s)je (a|i|o|u|e|y|é|è|à|û|ô|î|â|ê|œ|A|I|O|U|E|Y|É|È|À|Û|Ô|Î|Â|Ê|Œ)', r"\1j' \2", sentence)
        sentence = regex.sub(r'(^|\s)Je (a|i|o|u|e|y|é|è|à|û|ô|î|â|ê|œ|A|I|O|U|E|Y|É|È|À|Û|Ô|Î|Â|Ê|Œ)', r"\1J' \2", sentence)

        sentence = regex.sub(r'(^|\s)à le($|\s)', r'\1au\2', sentence)
        sentence = regex.sub(r'(^|\s)À le($|\s)', r'\1Au\2', sentence)

        sentence = regex.sub(r'(^|\s)à lequel($|\s)', r'\1auquel\2', sentence)
        sentence = regex.sub(r'(^|\s)À lequel($|\s)', r'\1Auquel\2', sentence)

        sentence = regex.sub(r'(^|\s)à les($|\s)', r'\1aux\2', sentence)
        sentence = regex.sub(r'(^|\s)À les($|\s)', r'\1Aux\2', sentence)

        sentence = regex.sub(r'(^|\s)à lesquels($|\s)', r'\1auxquels\2', sentence)
        sentence = regex.sub(r'(^|\s)À lesquels($|\s)', r'\1Auxquels\2', sentence)

        sentence = regex.sub(r'(^|\s)à lesquelles($|\s)', r'\1auxquelles\2', sentence)
        sentence = regex.sub(r'(^|\s)À lesquelles($|\s)', r'\1Auxquelles\2', sentence)

        sentence = regex.sub(r'(^|\s)de le($|\s)', r'\1du\2', sentence)
        sentence = regex.sub(r'(^|\s)De le($|\s)', r'\1Du\2', sentence)

        sentence = regex.sub(r'(^|\s)de lequel($|\s)', r'\1duquel\2', sentence)
        sentence = regex.sub(r'(^|\s)De lequel($|\s)', r'\1Duquel\2', sentence)

        sentence = regex.sub(r'(^|\s)de les($|\s)', r'\1des\2', sentence)
        sentence = regex.sub(r'(^|\s)De les($|\s)', r'\1Des\2', sentence)

        sentence = regex.sub(r'(^|\s)de lesquels($|\s)', r'\1desquels\2', sentence)
        sentence = regex.sub(r'(^|\s)De lesquels($|\s)', r'\1Desquels\2', sentence)

        sentence = regex.sub(r'(^|\s)de lesquelles($|\s)', r'\1desquelles\2', sentence)
        sentence = regex.sub(r'(^|\s)De lesquelles($|\s)', r'\1Desquelles\2', sentence)

        # sentence = regex.sub(r'(^|\s)la (\w)($|\s)', r'\1l\'\2\3', sentence)

        # jusqu'à l'arrivée
        # l'huile

        return sentence

