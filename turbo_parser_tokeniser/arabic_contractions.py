# -*- coding: utf-8 -*-
"""
The code for Arabic contractions was kindly written by Dr. Ahmed Abdelali <aabdelali@hbku.edu.qa>
"""

import re
from turbo_parser_tokeniser.contractions import Contractions


class ArabicContractions(Contractions):

    def __init__(self):
        pass

    def realise_surface_form(self, sentence):

        line = ' ' + sentence.strip() + ' '
        # print(line)

        pattern = re.compile(r'\ (ب|س|ف|ك|ل|و|ام|ان)\ +([^\ ]+)\ ')
        match = re.search(pattern, line)

        while match is not None:
            line = re.sub(pattern, ' \g<1>\g<2> ', line)
            match = re.search(pattern, line)

        # print(line)

        pattern = re.compile(r'\ ([^\ ]*?)\ +(ك|ه|ي|تم|كم|ما|نا|ها|ني|هم|هن|هما)\ ')
        match = re.search(pattern, line)
        while match is not None:
            line = re.sub(pattern, ' \g<1>\g<2> ', line)
            match = re.search(pattern, line)

        line = re.sub('\ لال([^\ ])', ' لل\g<1>', line)

        # print(line.strip())
        # print('في البداية اعتقد كنت انه لم يرث سوى 25 الف دولار ورفض في حينه التوجه الى اوهايو للمطالبة بارثه')

        return line.strip() + '\n'
