# -*- coding: utf-8 -*-

import regex
import os
import codecs
from turbo_parser_tokeniser.contractions import Contractions


class ItalianContractions(Contractions):
    def __init__(self):
        # Load Italian verbs and their inflections from a lexicon.
        filepath = os.sep.join([os.path.dirname(os.path.realpath(__file__)),
                                'italian_verbs.txt'])
        '''self.verbs = set()
        with codecs.open(filepath, encoding='utf8') as f:
            for line in f:
                fields = line.rstrip('\n').split()
                assert len(fields) == 3
                self.verbs.add(fields[0])'''

    def split_if_contraction(self, word):
        original_word = word

        # Handle preposition+determiner contractions.
        word = regex.sub(u'^([A|a])l$', u'\1 il', word)
        word = regex.sub(u'^([A|a])llo$', u'\1 lo', word)
        word = regex.sub(u'^([A|a])i$', u'\1 i', word)
        word = regex.sub(u'^([A|a])gli$', u'\1 gli', word)
        word = regex.sub(u'^([A|a])lla$', u'\1 la', word)
        word = regex.sub(u'^([A|a])lle$', u'\1 le', word)
        word = regex.sub(u'^([A|a])ll\'$', u"\1 l'", word)

        word = regex.sub(u'^([C|c])ol$', u'\1on il', word)
        word = regex.sub(u'^([C|c])oi$', u'\1on i', word)
        word = regex.sub(u'^([C|c])ogli$', u'\1on gli', word)
        word = regex.sub(u'^([C|c])olla$', u'\1on la', word)
        word = regex.sub(u'^([C|c])olle$', u'\1on le', word)
        word = regex.sub(u'^([C|c])oll\'$', u"\1on l'", word) # Not very used.

        word = regex.sub(u'^([D|d])al$', u'\1a il', word)
        word = regex.sub(u'^([D|d])allo$', u'\1a lo', word)
        word = regex.sub(u'^([D|d])ai$', u'\1a i', word)
        word = regex.sub(u'^([D|d])agli$', u'\1a gli', word)
        word = regex.sub(u'^([D|d])alla$', u'\1a la', word)
        word = regex.sub(u'^([D|d])alle$', u'\1a le', word)
        word = regex.sub(u'^([D|d])all\'$', u"\1a l'", word)

        word = regex.sub(u'^([D|d])el$', u'\1i il', word)
        word = regex.sub(u'^([D|d])ello$', u'\1i lo', word)
        word = regex.sub(u'^([D|d])ei$', u'\1i i', word)
        word = regex.sub(u'^([D|d])egli$', u'\1i gli', word)
        word = regex.sub(u'^([D|d])ella$', u'\1i la', word)
        word = regex.sub(u'^([D|d])elle$', u'\1i le', word)
        word = regex.sub(u'^([D|d])ell\'$', u"\1i l'", word)
        word = regex.sub(u'^([D|d])e\'$', u'\1i i', word) # Not very used.

        word = regex.sub(u'^Nel$', u'In il', word)
        word = regex.sub(u'^Nello$', u'In lo', word)
        word = regex.sub(u'^Nei$', u'In i', word)
        word = regex.sub(u'^Negli$', u'In gli', word)
        word = regex.sub(u'^Nella$', u'In la', word)
        word = regex.sub(u'^Nelle$', u'In le', word)
        word = regex.sub(u'^Nell\'$', u"In l'", word)

        word = regex.sub(u'^nel$', u'in il', word)
        word = regex.sub(u'^nello$', u'in lo', word)
        word = regex.sub(u'^nei$', u'in i', word)
        word = regex.sub(u'^negli$', u'in gli', word)
        word = regex.sub(u'^nella$', u'in la', word)
        word = regex.sub(u'^nelle$', u'in le', word)
        word = regex.sub(u'^nell\'$', u"in l'", word)

        word = regex.sub(u'^([P|p])el$', u'\1er il', word) # Not used.
        word = regex.sub(u'^([P|p])ei$', u'\1er i', word) # Not used.

        word = regex.sub(u'^([S|s])ul$', u'\1u il', word)
        word = regex.sub(u'^([S|s])ullo$', u'\1u lo', word)
        word = regex.sub(u'^([S|s])ui$', u'\1u i', word)
        word = regex.sub(u'^([S|s])ugli$', u'\1u gli', word)
        word = regex.sub(u'^([S|s])ulla$', u'\1u la', word)
        word = regex.sub(u'^([S|s])ulle$', u'\1u le', word)
        word = regex.sub(u'^([S|s])ull\'$', u"\1u l'", word)

        if original_word != word:
            return word

        # Handle clitics.
        word = regex.sub(u'^(ecco)(mi|ti|si|ci|vi|le|lo|la|li|ne|l\')$', \
                         u'\1 \2', word)
        word = regex.sub(u'^(glie)(le|lo|la|li|ne|l\')$', \
                         u'\1 \2', word)
        # Removed "or", "ta", "to", "ti" below because it was catching a lot of
        # nouns (e.g. "aeroporti", "abitati", "cartone", "aristocratici").
        # Was:
        # word = regex.sub( \
        #    u'(ar|er|ir|or|ur|ndo|ta|to|te|ti)' \
        #      '(mi|ti|si|ci|vi|gli|le|lo|la|li|ne)$', \
        #    u'\1 \2', word)
        word = regex.sub( \
           u'(ar|er|ir|ppor|^por|ur|ndo|te)' \
             '(mi|ti|si|ci|vi|gli|le|lo|la|li|ne)$', \
           u'\1 \2', word)
        word = regex.sub( \
            u'(ar|er|ir|or|ur|ndo|ta|to|te|ti)(me|te|se|ce|ve)'\
              '(gli|le|lo|la|li|ne)$', \
            u'\1 \2 \3', word)
        word = regex.sub(u'(ar|er|ir|or|ur|ndo)(glie)(le|lo|la|li|ne)$', \
                         u'\1 \2 \3', word)

        # If the first token is not a verb in the lexicon, put back the
        # original word. This is to avoid a lot of false positives that
        # are caught with the regexes above.
        if original_word != word:
            if word.split(' ')[0] not in self.verbs:
                word = original_word
            elif original_word in self.verbs:
                word = original_word

        return word

    def realise_surface_form(self, sentence):
        # Handle preposition+determiner contractions.

        sentence = regex.sub(r'(^|\s)([A|a]) il($|\s)', r'\1\2l\3', sentence)
        sentence = regex.sub(r'(^|\s)([A|a]) lo($|\s)', r'\1\2llo\3', sentence)
        sentence = regex.sub(r'(^|\s)([A|a]) i($|\s)', r'\1\2i\3', sentence)
        sentence = regex.sub(r'(^|\s)([A|a]) gli($|\s)', r'\1\2gli\3', sentence)
        sentence = regex.sub(r'(^|\s)([A|a]) la($|\s)', r'\1\2lla\3', sentence)
        sentence = regex.sub(r'(^|\s)([A|a]) le($|\s)', r'\1\2lle\3', sentence)
        sentence = regex.sub(r"(^|\s)([A|a]) l'($|\s)", r"\1\2ll'\3", sentence)

        '''
        sentence = regex.sub(u'^([A|a])l$', u'\1 il', sentence)
        sentence = regex.sub(u'^([A|a])llo$', u'\1 lo', sentence)
        sentence = regex.sub(u'^([A|a])i$', u'\1 i', sentence)
        sentence = regex.sub(u'^([A|a])gli$', u'\1 gli', sentence)
        sentence = regex.sub(u'^([A|a])lla$', u'\1 la', sentence)
        sentence = regex.sub(u'^([A|a])lle$', u'\1 le', sentence)
        sentence = regex.sub(u'^([A|a])ll\'$', u"\1 l'", sentence)'''

        sentence = regex.sub(r'(^|\s)([C|c])on il($|\s)', r'\1\2ol\3', sentence)
        sentence = regex.sub(r'(^|\s)([C|c])on i($|\s)', r'\1\2oi\3', sentence)
        sentence = regex.sub(r'(^|\s)([C|c])on gli($|\s)', r'\1\2ogli\3', sentence)
        sentence = regex.sub(r'(^|\s)([C|c])on la($|\s)', r'\1\2olla\3', sentence)
        sentence = regex.sub(r'(^|\s)([C|c])on le($|\s)', r'\1\2olle\3', sentence)
        sentence = regex.sub(r"(^|\s)([C|c])on l'($|\s)", r"\1\2oll'\3", sentence)


        '''sentence = regex.sub(u'^([C|c])ol$', u'\1on il', sentence)
        sentence = regex.sub(u'^([C|c])oi$', u'\1on i', sentence)
        sentence = regex.sub(u'^([C|c])ogli$', u'\1on gli', sentence)
        sentence = regex.sub(u'^([C|c])olla$', u'\1on la', sentence)
        sentence = regex.sub(u'^([C|c])olle$', u'\1on le', sentence)
        sentence = regex.sub(u'^([C|c])oll\'$', u"\1on l'", sentence) # Not very used.'''

        sentence = regex.sub(r'(^|\s)([D|d])a il($|\s)', r'\1\2al\3', sentence)
        sentence = regex.sub(r'(^|\s)([D|d])a lo($|\s)', r'\1\2allo\3', sentence)
        sentence = regex.sub(r'(^|\s)([D|d])a i($|\s)', r'\1\2ai\3', sentence)
        sentence = regex.sub(r'(^|\s)([D|d])a gli($|\s)', r'\1\2agli\3', sentence)
        sentence = regex.sub(r'(^|\s)([D|d])a la($|\s)', r'\1\2alla\3', sentence)
        sentence = regex.sub(r'(^|\s)([D|d])a le($|\s)', r'\1\2alle\3', sentence)
        sentence = regex.sub(r"(^|\s)([D|d])a l'($|\s)", r"\1\2all'\3", sentence)

        '''sentence = regex.sub(u'^([D|d])al$', u'\1a il', sentence)
        sentence = regex.sub(u'^([D|d])allo$', u'\1a lo', sentence)
        sentence = regex.sub(u'^([D|d])ai$', u'\1a i', sentence)
        sentence = regex.sub(u'^([D|d])agli$', u'\1a gli', sentence)
        sentence = regex.sub(u'^([D|d])alla$', u'\1a la', sentence)
        sentence = regex.sub(u'^([D|d])alle$', u'\1a le', sentence)
        sentence = regex.sub(u'^([D|d])all\'$', u"\1a l'", sentence)'''

        sentence = regex.sub(r'(^|\s)([D|d])i il($|\s)', r'\1\2el\3', sentence)
        sentence = regex.sub(r'(^|\s)([D|d])i lo($|\s)', r'\1\2ello\3', sentence)
        sentence = regex.sub(r'(^|\s)([D|d])i i($|\s)', r'\1\2ei\3', sentence)
        sentence = regex.sub(r'(^|\s)([D|d])i gli($|\s)', r'\1\2egli\3', sentence)
        sentence = regex.sub(r'(^|\s)([D|d])i la($|\s)', r'\1\2ella\3', sentence)
        sentence = regex.sub(r'(^|\s)([D|d])i le($|\s)', r'\1\2elle\3', sentence)
        sentence = regex.sub(r"(^|\s)([D|d])i l'($|\s)", r"\1\2ell'\3", sentence)

        '''sentence = regex.sub(u'^([D|d])el$', u'\1i il', sentence)
        sentence = regex.sub(u'^([D|d])ello$', u'\1i lo', sentence)
        sentence = regex.sub(u'^([D|d])ei$', u'\1i i', sentence)
        sentence = regex.sub(u'^([D|d])egli$', u'\1i gli', sentence)
        sentence = regex.sub(u'^([D|d])ella$', u'\1i la', sentence)
        sentence = regex.sub(u'^([D|d])elle$', u'\1i le', sentence)
        sentence = regex.sub(u'^([D|d])ell\'$', u"\1i l'", sentence)
        sentence = regex.sub(u'^([D|d])e\'$', u'\1i i', sentence) # Not very used.'''

        sentence = regex.sub(r'(^|\s)In il($|\s)', r'\1Nel\2', sentence)
        sentence = regex.sub(r'(^|\s)in il($|\s)', r'\1nel\2', sentence)

        sentence = regex.sub(r'(^|\s)In lo($|\s)', r'\1\Nello\2', sentence)
        sentence = regex.sub(r'(^|\s)in lo($|\s)', r'\1\nello\2', sentence)

        sentence = regex.sub(r'(^|\s)In i($|\s)', r'\1Nei\2', sentence)
        sentence = regex.sub(r'(^|\s)in i($|\s)', r'\1nei\2', sentence)

        sentence = regex.sub(r'(^|\s)In gli($|\s)', r'\1Negli\2', sentence)
        sentence = regex.sub(r'(^|\s)in gli($|\s)', r'\1negli\2', sentence)

        sentence = regex.sub(r'(^|\s)In la($|\s)', r'\1Nella\2', sentence)
        sentence = regex.sub(r'(^|\s)in la($|\s)', r'\1nella\2', sentence)

        sentence = regex.sub(r'(^|\s)In le($|\s)', r'\1Nelle\2', sentence)
        sentence = regex.sub(r'(^|\s)in le($|\s)', r'\1nelle\2', sentence)

        sentence = regex.sub(r"(^|\s)In l'($|\s)", r"\1Nell'\2", sentence)
        sentence = regex.sub(r"(^|\s)in l'($|\s)", r"\1nell'\2", sentence)


        '''sentence = regex.sub(u'^Nel$', u'In il', sentence)
        sentence = regex.sub(u'^Nello$', u'In lo', sentence)
        sentence = regex.sub(u'^Nei$', u'In i', sentence)
        sentence = regex.sub(u'^Negli$', u'In gli', sentence)
        sentence = regex.sub(u'^Nella$', u'In la', sentence)
        sentence = regex.sub(u'^Nelle$', u'In le', sentence)
        sentence = regex.sub(u'^Nell\'$', u"In l'", sentence)

        sentence = regex.sub(u'^nel$', u'in il', sentence)
        sentence = regex.sub(u'^nello$', u'in lo', sentence)
        sentence = regex.sub(u'^nei$', u'in i', sentence)
        sentence = regex.sub(u'^negli$', u'in gli', sentence)
        sentence = regex.sub(u'^nella$', u'in la', sentence)
        sentence = regex.sub(u'^nelle$', u'in le', sentence)
        sentence = regex.sub(u'^nell\'$', u"in l'", sentence)'''

        # sentence = regex.sub(r'(^|\s)([P|p])er il($|\s)', r'\1\2el\3', sentence) # Not used.
        # sentence = regex.sub(r'(^|\s)([P|p])er i($|\s)', r'\1\2ei\3', sentence) # Not used.

        '''sentence = regex.sub(u'^([P|p])el$', u'\1er il', sentence) # Not used.
        sentence = regex.sub(u'^([P|p])ei$', u'\1er i', sentence) # Not used.'''

        sentence = regex.sub(r'(^|\s)([S|s])u il($|\s)', r'\1\2ul\3', sentence)
        sentence = regex.sub(r'(^|\s)([S|s])u lo($|\s)', r'\1\2ullo\3', sentence)
        sentence = regex.sub(r'(^|\s)([S|s])u i($|\s)', r'\1\2ui\3', sentence)
        sentence = regex.sub(r'(^|\s)([S|s])u gli($|\s)', r'\1\2ugli\3', sentence)
        sentence = regex.sub(r'(^|\s)([S|s])u la($|\s)', r'\1\2ulla\3', sentence)
        sentence = regex.sub(r'(^|\s)([S|s])u le($|\s)', r'\1\2ulle\3', sentence)
        sentence = regex.sub(r"(^|\s)([S|s])u l'($|\s)", r"\1\2ull'\3", sentence)

        '''sentence = regex.sub(u'^([S|s])ul$', u'\1u il', sentence)
        sentence = regex.sub(u'^([S|s])ullo$', u'\1u lo', sentence)
        sentence = regex.sub(u'^([S|s])ui$', u'\1u i', sentence)
        sentence = regex.sub(u'^([S|s])ugli$', u'\1u gli', sentence)
        sentence = regex.sub(u'^([S|s])ulla$', u'\1u la', sentence)
        sentence = regex.sub(u'^([S|s])ulle$', u'\1u le', sentence)
        sentence = regex.sub(u'^([S|s])ull\'$', u"\1u l'", sentence)'''

        return sentence
