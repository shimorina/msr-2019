# -*- coding: utf-8 -*-

from conllu import parse_tree_incr
from conllu import parse_incr
from sacremoses import MosesDetokenizer


def read_conllu(filepath):
    sentences = []
    data_file = open(filepath, "r", encoding="utf-8")
    for tokenlist in parse_tree_incr(data_file):
        sentences.append(tokenlist)
    return sentences


def read_conllu_linear(filepath):
    sentences = []
    data_file = open(filepath, "r", encoding="utf-8")
    for tokenlist in parse_incr(data_file):
        sentences.append(tokenlist)
    return sentences


def file_to_list(filepath):
    with open(filepath, 'r', encoding="utf-8") as f:
        corpus = f.readlines()
    return corpus


def list_to_file(data_list, filepath):
    with open(filepath, 'w+', encoding="utf-8") as f:
        f.write('\n'.join(data_list) + '\n')


def read_original_conllu(filepath_orig, lang, form=True):
    """
    Read original UD-v2.3 dataset.
    :param filepath_orig: original UD conllu file
    :param lang: language
    :param form: take 'form' or 'lemma' column in conll
    :return: lowercased tokenised sentences for training, uppercase detokenised sentences for evaluation.
    """
    sentences = []  # lowercase, tokenised
    sentences_upper_detok = []  # uppercase, detokenised
    with open(filepath_orig, 'r', encoding='utf-8') as f:
        for tokenlist in parse_incr(f):
            sent = []
            for token in tokenlist:
                if form:
                    sent += [token['form']]
                else:
                    if lang == 'pt_1' and token['lemma'] == '_':  # todo: to generate correct lemma for pt_gsd
                        sent += [token['form']]
                    else:
                        sent += [token['lemma']]
            sentences += [' '.join(sent)]
            sentences_upper_detok += [detokenise(sent, lang)]
    # print('original ud size', len(sentences))
    return sentences_upper_detok, sentences


def detokenise(data, lang):
    """
    Detokenisation
    :param data: list of tokens
    :param lang: language
    :return: detokenised string
    """
    detokenizer = MosesDetokenizer(lang=lang)
    return detokenizer.detokenize(data, return_str=True)
