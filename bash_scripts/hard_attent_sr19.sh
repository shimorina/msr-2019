#!/bin/bash
source config.sh

### Train MR module and output morphological forms for test data

datapath=${global}/morphology/data_for_reinflection/
resultpath=${global}/morphology/results-sr19/

# no morphology needed: ja ko zh

for lang in ar es id fr hi pt ru en
do
	mkdir -p $resultpath/$lang-200-200-adam
	python ${mr}/hard_attention.py \
	--dynet-mem 4096 \
	--input=200 \
	--hidden=200 \
	--feat-input=20 \
	--epochs=20 \
	--layers=2 \
	--dynet-gpu 0 \
	${datapath}/${lang}-task1-freq-train \
	${datapath}/${lang}-task1-freq-short-dev \
	${datapath}/${lang}-task1-one-occurr-test \
	${resultpath}/${lang}-200-200-adam/${lang} \
	${sigmorphonpath}

done
