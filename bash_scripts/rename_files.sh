#!/bin/bash

source config.sh

outputs=${global}/outputs/templates/test/
mkdir ${outputs}/to_send
mkdir ${outputs}/to_send/T1-tokenised
mkdir ${outputs}/to_send/T1-detokenised
cp ${outputs}/*_out.tok.txt ${outputs}/to_send/T1-tokenised
cp ${outputs}/*_out.txt ${outputs}/to_send/T1-detokenised

for input in tokenised detokenised
	do
		tgtpath=${outputs}/to_send/T1-${input}
		mv ${tgtpath}/ar_out.*txt ${tgtpath}/ar_padt-ud-test.txt

		mv ${tgtpath}/es_1_out.*txt ${tgtpath}/es_ancora-ud-test.txt
		mv ${tgtpath}/es_2_out.*txt ${tgtpath}/es_gsd-ud-test.txt
		mv ${tgtpath}/es_3_out.*txt ${tgtpath}/es_ancora-Pred-HIT.txt

		mv ${tgtpath}/en_1_out.*txt ${tgtpath}/en_ewt-ud-test.txt
		mv ${tgtpath}/en_2_out.*txt ${tgtpath}/en_gum-ud-test.txt
		mv ${tgtpath}/en_3_out.*txt ${tgtpath}/en_lines-ud-test.txt
		mv ${tgtpath}/en_4_out.*txt ${tgtpath}/en_partut-ud-test.txt
		mv ${tgtpath}/en_5_out.*txt ${tgtpath}/en_ewt-Pred-HIT-edit.txt
		mv ${tgtpath}/en_6_out.*txt ${tgtpath}/en_pud-Pred-LATTICE.txt
		mv ${tgtpath}/en_7_out.*txt ${tgtpath}/en_pud-ud-test.txt

		mv ${tgtpath}/id_out.*txt ${tgtpath}/id_gsd-ud-test.txt

		mv ${tgtpath}/fr_1_out.*txt ${tgtpath}/fr_gsd-ud-test.txt
		mv ${tgtpath}/fr_2_out.*txt ${tgtpath}/fr_sequoia-ud-test.txt
		mv ${tgtpath}/fr_3_out.*txt ${tgtpath}/fr_partut-ud-test.txt

		mv ${tgtpath}/hi_1_out.*txt ${tgtpath}/hi_hdtb-ud-test.txt
		mv ${tgtpath}/hi_2_out.*txt ${tgtpath}/hi_hdtb-Pred-HIT.txt

		mv ${tgtpath}/ja_1_out.*txt ${tgtpath}/ja_gsd-ud-test.txt
		mv ${tgtpath}/ja_2_out.*txt ${tgtpath}/ja_pud-ud-test.txt

		mv ${tgtpath}/ko_1_out.*txt ${tgtpath}/ko_kaist-ud-test.txt
		mv ${tgtpath}/ko_2_out.*txt ${tgtpath}/ko_gsd-ud-test.txt
		mv ${tgtpath}/ko_3_out.*txt ${tgtpath}/ko_kaist-Pred-HIT.txt

		mv ${tgtpath}/pt_1_out.*txt ${tgtpath}/pt_gsd-ud-test.txt
		mv ${tgtpath}/pt_2_out.*txt ${tgtpath}/pt_bosque-ud-test.txt
		mv ${tgtpath}/pt_3_out.*txt ${tgtpath}/pt_bosque-Pred-Stanford.txt

		mv ${tgtpath}/ru_1_out.*txt ${tgtpath}/ru_syntagrus-ud-test.txt
		mv ${tgtpath}/ru_2_out.*txt ${tgtpath}/ru_gsd-ud-test.txt
		mv ${tgtpath}/ru_3_out.*txt ${tgtpath}/ru_pud-ud-test.txt

		mv ${tgtpath}/zh_out.*txt ${tgtpath}/zh_gsd-ud-test.txt
	done
