#!/bin/bash
source config.sh

######### SR 19 ##############
track=shallow
part=test
basepath=${global}/opennmtpy_models/sr19/factored_models/

for lang in ar id zh en es fr ko pt ru hi ja
do
    prefix=${lang}_shallow_5factored
	modelpaths=($(ls -d ${basepath}/${prefix}*))
	for modelpath in ${modelpaths[@]}
    do
    	echo $modelpath
		path=${global}/processed_data/${track}/${lang}
		modelname=${lang}_${track}_5factored
		for epoch in 17
		do
			datasets=($(ls -d $path/${lang}*-ud-${part}.${track}.delex.5factored))
			for dataset in ${datasets[@]}
			do
				# extract lang_id
				echo $dataset
				lang_id=$(echo $dataset | rev | cut -d'/' -f1 | rev | cut -d'-' -f 1)  # split by "/" to find filename and then by "-" to find lang_id
				# echo $lang_id
				python3 ${opennmt}/translate.py \
				-model $modelpath/${modelname}*_e${epoch}.pt \
				-src $dataset \
				-output $modelpath/prediction/prediction-${part}-${lang_id}-ep${epoch}.txt \
				-gpu 0 \
				-beam_size 5 \
				-replace_unk \
				-dynamic_dict \
				-share_vocab \

			done
		done
	done
done
