#!/bin/bash
source config.sh
# merge datasets (en_1, en_2, en_3, en_4) and shuffle

for lang in en es fr pt ko ru  # only for langs which have multiple corpora
do
	datapath=${global}/processed_data/shallow/${lang}
	for track in train dev
	do
		cat ${datapath}/${lang}_*-ud-${track}.shallow.delex.5factored > ${datapath}/${lang}-ud-${track}.shallow.delex.5factored | shuf --random-source=/dev/zero
		cat ${datapath}/${lang}_*-ud-${track}.delex.lemma > ${datapath}/${lang}-ud-${track}.delex.lemma | shuf --random-source=/dev/zero
	done
done
