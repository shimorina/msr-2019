#!/bin/bash
source config.sh

## Word Ordering
echo "Preparing files for training (can take some time)"
python3 ${global}/delexicalisation_dict_creation.py
python3 ${global}/conll_to_factored.py
python3 ${global}/ud_to_string.py

./merge_files.sh
echo "Start training with OpenNMT"

./sr19_factored_fulldelex.sh  # decode on dev
./sr19_factored_fulldelex_test.sh

## Morphological Realisation + Contraction Generation
echo "Preparing files for morphology"
python3 ${global}/morphology/prepare_morph_for_training.py
python3 ${global}/morphology/prepare_morph_based_on_freq.py

echo "Start training morphology"

./hard_attent_sr19.sh
./hard_attent_decode_sr19.sh
echo "Preparing morphological dictionaries"
python3 ${global}/morphology/create_lemma_form_dicts.py

## Evaluation
echo "Starting evaluation"
./generate_templates.sh
./sr19_factored_fulldelex_lemma_eval.sh
./sr19_factored_fulldelex_form_gold_wo_eval.sh
./sr19_factored_fulldelex_form_eval.sh
./sr19_factored_fulldelex_form_test.sh
