#!/bin/bash

# path to the repository
global=/home/anastasia/Loria/msr-2019

# evaluation script
eval_script=/home/anastasia/Loria/thesis/sr_19/eval_Py3_v2.py

# data

# model outputs
basepath=${global}/opennmtpy_models/sr19/factored_models

# OpenNMT-py dir
opennmt=/home/anastasia/OpenNMT-py

# Morphological inflection
mr=/home/anastasia/Loria/morphological-reinflection/src

# for MR module installation
sigmorphonpath=/home/anastasia/Loria/sigmorphon2016/
