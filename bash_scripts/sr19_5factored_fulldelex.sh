#!/bin/bash
source config.sh

######### SR 19 ##############
track=shallow
part=dev  # on which part to test

for lang in es fr ko pt ru ar id hi ja zh en
do
    # seeds=($(shuf -i 1-1000 -n 3))   # uncomment this block if you would like to run each language with three random seeds.
    # for seed in ${seeds[@]}
    # do
		path=${global}/processed_data/${track}/${lang}/
		modelname=${lang}_${track}_5factored
		modelpath=${global}/opennmtpy_models/sr19/factored_models/${modelname}
		mkdir -p ${modelpath}

		# preprocess
		python3 ${opennmt}/preprocess.py \
		-train_src ${path}/${lang}-ud-train.${track}.delex.5factored \
		-train_tgt ${path}/${lang}-ud-train.delex.lemma \
		-valid_src ${path}/${lang}-ud-dev.${track}.delex.5factored \
		-valid_tgt ${path}/${lang}-ud-dev.delex.lemma \
		-save_data $modelpath/$modelname \
		-dynamic_dict \
		-share_vocab \
		-src_vocab_size 1000 \
		-src_seq_length 150 \
		-tgt_seq_length 150 \

		# train
		python3 ${opennmt}/train.py \
		-data $modelpath/$modelname \
		-save_model $modelpath/$modelname \
		-word_vec_size 300 \
		-gpuid 0 \
		-copy_attn \
		-copy_attn_force \
		-coverage_attn \
		-epochs 20 \
		-layers 1 \
		-start_decay_at 12 \
		-rnn_size 450 \
		-share_embeddings \
		# -seed ${seed} \

		mkdir -p $modelpath/prediction

		python3 ${opennmt}/translate.py \
		-model $modelpath/$modelname_*_e17.pt \
		-src $path/${lang}-ud-${part}.${track}.delex.5factored \
		-output $modelpath/prediction/prediction-${part}-ep17.txt \
		-gpu 0 \
		-beam_size 5 \
		-replace_unk \
		-dynamic_dict \
		-share_vocab \

	# done
done
