#!/bin/bash
source config.sh

### Generate reference files put into templates

refdevpath=${global}/processed_data/templates/
processed=${global}/processed_data/shallow

# generate reference files with sentences
for lang in en_1 en_2 en_3 en_4 ar es_1 es_2 fr_1 fr_2 fr_3 pt_1 pt_2 ru_1 ru_2 ko_1 ko_2 id ja zh
do 
	part=dev
	lang_short=$(echo $lang | cut -d'_' -f 1)
	filepath=${processed}/${lang_short}/${lang}-ud-${part}.sent
	python3 ${global}/generate_templates.py ${filepath} ${part} ${lang} ${refdevpath} --detok_input

done


# use tokens from UD for ja and zh
for lang in ja zh hi
do 
	part=dev
	lang_short=$(echo $lang | cut -d'_' -f 1)
	filepath=${processed}/${lang_short}/${lang}-ud-${part}.token
	python3 ${global}/generate_templates.py ${filepath} ${part} ${lang} ${refdevpath} --token_input

done

# ko_out.tok.txt -- doesn't make sense
# ko_out.txt -- should be used



# generate reference files with lemmas
for lang in en_1 en_2 en_3 en_4 ar es_1 es_2 fr_1 fr_2 fr_3 pt_1 pt_2 ru_1 ru_2 ko_1 ko_2 id ja zh hi
do 
	part=dev
	lang_short=$(echo $lang | cut -d'_' -f 1)
	filepath=${processed}/${lang_short}/${lang}-ud-${part}.lemma
	python3 ${global}/generate_templates.py ${filepath} dev_lemmas ${lang} ${refdevpath} --token_input

done

