#!/bin/bash
source config.sh

### Produce outputs for dev (only word order) and evaluate against sequences of lemmas.

# references
refdevpath=${global}/processed_data/templates/dev_lemmas/
# where to write templated system outputs
output_template_dir=${global}/outputs/templates/
		

for lang in en_1 ru_2 ru_1 ru_2 ar hi id ja zh es_1 es_2 en_1 en_2 en_3 en_4 fr_1 fr_2 fr_3 pt_1 pt_2 ko_1 ko_2 
do 
	part=dev
	lang_short=$(echo $lang | cut -d'_' -f 1)
	echo $lang

	# gather random seeds if any
	prefix=${lang_short}_shallow_5factored
	modelpaths=($(ls -d ${basepath}/${prefix}*/prediction))
	for modelpath in ${modelpaths[@]}
	do
		#echo ${modelpath}
		predfile=prediction-${part}-${lang}-ep17.deanon.txt
		deanondict=${global}/delex_dicts/anonym-id-lemma-dict-${lang}-${part}.txt

		# # deanonymise the prediction file and inflect: -dictionary -prediction -lang -part -inflect(true by default) -contract (true by default)
		python3 ${global}/deanonymise.py \
		${deanondict} \
		${modelpath}/prediction-${part}-${lang}-ep17.txt \
		${lang} \
		-p ${part} \
		--no-inflect \
		--no-contract

		# detokenise; prepare templates; save template (tokenised) in output_template_dir
		python3 ${global}/generate_templates.py \
		${modelpath}/${predfile} \
		dev_lemmas \
		${lang} \
		${output_template_dir} \
		--token_input

	done
done

python3 ${eval_script} ${output_template_dir}/dev_lemmas ${refdevpath}
python3 ${eval_script} ${output_template_dir}/dev_lemmas ${refdevpath} > bleu.dist.lemma.dev
