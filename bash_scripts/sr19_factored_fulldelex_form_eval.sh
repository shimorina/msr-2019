#!/bin/bash
source config.sh

### Produce outputs for dev and evaluate against reference sentences.
# references
refdevpath=${global}/processed_data/templates/dev/
# where store outputs
output_template_dir=${global}/outputs/templates/
		

for lang in fr_1 fr_2 fr_3 ru_2 ru_1 ar id ja zh es_1 es_2 en_1 en_2 en_3 en_4 pt_1 pt_2 ko_1 ko_2 hi
do 
	part=dev
	lang_short=$(echo $lang | cut -d'_' -f 1)
	echo $lang

	# gather random seeds if any
	prefix=${lang_short}_shallow_5factored
	modelpaths=($(ls -d ${basepath}/${prefix}*/prediction))
	for modelpath in ${modelpaths[@]}
	do
		#echo ${modelpath}
		deanondict=${global}/delex_dicts/anonym-id-lemma-dict-${lang}-${part}.txt

		predfile=prediction-${part}-${lang}-ep17.deanon.inflect.contract.txt
		# # deanonymise the prediction file and inflect: -dictionary -prediction -lang -part -inflect(true by default) -contract (true by default)
		python3 ${global}/deanonymise.py \
		${deanondict} \
		${modelpath}/prediction-${part}-${lang}-ep17.txt \
		${lang} \
		-p ${part}

		# predfile=prediction-${part}-${lang}-ep17.deanon.inflect.txt  only WO+MR (without contraction)
		# python3 ${global}/deanonymise.py \
		# ${deanondict} \
		# ${modelpath}/prediction-${part}-${lang}-ep17.txt \
		# ${lang} \
		# -p ${part} \
		# --no-contract \

		# detokenise; prepare templates; save template (tokenised and detokenised) in output_template_dir
		python3 ${global}/generate_templates.py \
		${modelpath}/${predfile} \
		${part} \
		${lang} \
		${output_template_dir} \
		--token_input
		
	done
done
# script [system-dir] [reference-dir]
python3 ${eval_script} ${output_template_dir}/${part} ${refdevpath}
python3 ${eval_script} ${output_template_dir}/${part} ${refdevpath} > bleu.dist.form.dev
