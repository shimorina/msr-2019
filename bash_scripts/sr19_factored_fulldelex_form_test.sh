#!/bin/bash
source config.sh

### Produce outputs for test.
output_template_dir=${global}/outputs/templates/
		

for lang in ar hi_1 hi_2 id ja_1 ja_2 zh es_1 es_2 es_3 en_1 en_2 en_3 en_4 en_5 en_6 en_7 fr_1 fr_2 fr_3 pt_1 pt_2 pt_3 ko_1 ko_2 ko_3 ru_1 ru_2 ru_3
do 
	part=test
	lang_short=$(echo $lang | cut -d'_' -f 1)
	echo $lang

	# gather random seeds if any
	prefix=${lang_short}_shallow_5factored
	modelpaths=($(ls -d ${basepath}/${prefix}*/prediction))
	for modelpath in ${modelpaths[@]}
	do
		echo ${modelpath}
		predfile=prediction-${part}-${lang}-ep17.deanon.inflect.contract.txt
		deanondict=${global}/delex_dicts/anonym-id-lemma-dict-${lang}-${part}.txt

		# # deanonymise the prediction file and inflect: -dictionary -prediction -lang -part(dev by default) -inflect(true by default) -contract (true by default)
		python3 ${global}/deanonymise.py \
		${deanondict} \
		${modelpath}/prediction-${part}-${lang}-ep17.txt \
		${lang} \
		-p ${part}

		# detokenise; prepare templates; save template (tokenised and detokenised) in output_template_dir
		python3 ${global}/generate_templates.py \
		${modelpath}/${predfile} \
		${part} \
		${lang} \
		${output_template_dir} \
		--token_input
		
	done
done
