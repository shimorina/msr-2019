#!/bin/bash
source config.sh

### Produce outputs for dev from gold word order (to evaluate MR sepately).

basepath=${global}/processed_data/shallow/ # we override basepath, since we take lemma sequences from gold data
refdevpath=${global}/processed_data/templates/dev/
output_template_dir=${global}/outputs/templates/
		

for lang in ru_2 ru_1 ru_2 ar hi id ja zh es_1 es_2 en_1 en_2 en_3 en_4 fr_1 fr_2 fr_3 pt_1 pt_2 ko_1 ko_2 
do 
	part=dev
	lang_short=$(echo $lang | cut -d'_' -f 1)
	echo $lang

	modelpath=${basepath}/${lang_short}/
	deanondict=${global}/delex_dicts/anonym-id-lemma-dict-${lang}-${part}.txt

	# # deanonymise the prediction file and inflect: -dictionary -prediction -lang -part -inflect(true by default) -contract (true by default)
	python3 ${global}/deanonymise.py \
	${deanondict} \
	${modelpath}/${lang}-ud-${part}.delex.lemma \
	${lang} \
	-p ${part} # --no-contract

	# the previous line generates this file
	predfile=${lang}-ud-${part}.delex.l.deanon.inflect.contract.txt
	# move it to another dir and rename
	newpredfile=${global}/outputs/pred_from_gold_wo/gold-wo-${lang}-ud-${part}.deanon.inflect.contract.txt
	mv ${modelpath}/${predfile} ${newpredfile}

	# detokenise; prepare templates; save template (tokenised and detokenised) in evaldir_test
	python3 ${global}/generate_templates.py \
	${newpredfile} \
	dev_from_gold_wo \
	${lang} \
	${output_template_dir} \
	--token_input

done

# script [system-dir] [reference-dir]
python3 ${eval_script} ${output_template_dir}/dev_from_gold_wo ${refdevpath}
python3 ${eval_script} ${output_template_dir}/dev_from_gold_wo ${refdevpath} > bleu.dist.form.gold_wo.dev
