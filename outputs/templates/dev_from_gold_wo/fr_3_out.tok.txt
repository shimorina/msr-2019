#sent_id = 1
#text = Creative Commons n' est pas un cabinet d' avocats et ne fournit pas des services de conseil juridique .

#sent_id = 2
#text = L' oeuvre ( telle que définie ci-dessous ) est mise à disposition selon les termes de l' présent contrat appelé Contrat Public Creative Commons ( dénommé ici « CPCC » ou « contrat » ) .

#sent_id = 3
#text = 1 ) définitions .

#sent_id = 4
#text = 4 ) restrictions .

#sent_id = 5
#text = 7 ) Résiliation .

#sent_id = 6
#text = Vous avez souhaité un débat à ce sujet dans les prochains jours , au cours de cette période de session .

#sent_id = 7
#text = Le paragraphe 6 de l' rapport Cunha sur les programmes d' orientation pluriannuels , qui sera soumis au parlement ce jeudi , propose d' introduire des sanctions applicables aux pays qui ne respectent pas les objectifs annuels de réduction de leur flotte .

#sent_id = 8
#text = Cher collègue nous allons vérifier tout cela .

#sent_id = 9
#text = Je voudrais encore aborder un dernier point :

#sent_id = 10
#text = Tout en sachant que c' n' est là qu' un petit pas vers une plus grande sécurité en matière de transport , je vous prie d' approuver ce rapport .

#sent_id = 11
#text = C' est la première fois que je prends la parole en plénière , il y a donc de quoi être un peu nerveux , un peu comme avec le premier amour , mais le premier amour a quand même durer heureusement plus de deux minutes .

#sent_id = 12
#text = Le contexte que connaissent nos différents états membres est , à bien des égard , divers .

#sent_id = 13
#text = Si m. Koch a rédigé son bon rapport , c' est parce que le travail n' a pas été exécuté de manière trop rapide au sein du CEN et de la commission économique des Nations Unies .

#sent_id = 14
#text = M. le président , je réitère de nouveau mes félicitations à m. Koch de l' travail qu' il a accompli pour c' autre rapport , qui vient compléter d' une certaine manière le débat que nous avons eu au mois d' octobre sur le transport par rail .

#sent_id = 15
#text = Il faut tenir compte de l' fait que l' espace rural représente presque quatre cinquièmes de l' territoire de l' Union Européenne .

#sent_id = 16
#text = Nous sommes maintenant très en retard .

#sent_id = 17
#text = Nous nous opposer à l' interventionnisme de l' administration centrale de l' union et de celle des états membres et nous demandons une réduction de la bureaucratie qui s' est incrustée dans l' élaboration et la réalisation des programmes .

#sent_id = 18
#text = Les fonds structurels ont joué un rôle fondamental dans le développement des zones urbaines et rurales des pays périphériques , principalement par l' amélioration des routes , de l' traitement de l' eau et des réseaux de transports afférent .

#sent_id = 19
#text = Comme pouvons nous faire en sorte que la politique de l' union s' articule avec les politiques nationales subsidiaires d' aménagement de l' territoire .

#sent_id = 20
#text = Pour les experts de l' plan français , par exemple , le scénario le plus probable est aujourd'hui un creusement des différenciations régionales à l' intérieur de chaque pays .

#sent_id = 21
#text = De plus , si vous choisissez d' utiliser Facebook à partir de votre téléphone mobile , noter que vous êtes responsable des frais associés à l' utilisation d' Internet et / ou aux SMS , selon les termes de votre opérateur de téléphonie mobile .

#sent_id = 22
#text = Pour vous inscrire et créer un tout nouveau compte , saisissez votre nom , votre date d' anniversaire , votre sexe et votre adresse électronique au format .

#sent_id = 23
#text = Publicités Facebook .

#sent_id = 24
#text = Approfondir vos relations .

#sent_id = 25
#text = Un taux de conversion de 10 % des visites provenir des publicités Facebook .

#sent_id = 26
#text = ( 21 ) les produits consommateurs d' énergie conformes aux exigences d' écoconception établies dans les mesures d' exécution de la présente directive doivent porter le marquage CE et les informations associées , afin de pouvoir être mis sur le marché intérieur et y circuler librement .

#sent_id = 27
#text = Et l' un des ateliers a abouti à une proposition de recommandation de l' conseil de la part des états membres .

#sent_id = 28
#text = Considérant que la reconnaissance de la dignité inhérente à tous les membres de la famille humaine et de leurs droits égaux et inaliénable constitue le fondement de la liberté , de la0 justice et de la paix dans le monde .

#sent_id = 29
#text = 1 . tout individu a droit à une nationalité .

#sent_id = 30
#text = C' est cette pyramide .

#sent_id = 31
#text = Et de quelle façon ?

#sent_id = 32
#text = Et ce prédateur suprême , c' est nous bien sûr .

#sent_id = 33
#text = J' aime les défis , et sauver la Terre est probablement un bon défis .

#sent_id = 34
#text = Nous savons tous que la Terre est en danger .

#sent_id = 35
#text = Nous sommes entrés dans le sixième X :

#sent_id = 36
#text = La sixième grande extinction de la planète .

#sent_id = 37
#text = Je pense que ce vote se déroule en ce moment même .

#sent_id = 38
#text = Je tiens à vous présenter une série de six solutions mycologiques qui utilisent les champignons , et ces solutions sont fondées sur le mycélium .

#sent_id = 39
#text = Le mycelium est présent dans tous les paysages , il retient les sols , il est extrêmement tenace .

#sent_id = 40
#text = Celui-ci retient jusqu' à 30000 fois sa masse .

#sent_id = 41
#text = Ce sont les grands désassembleurs moléculaire de la nature - les magiciens de l' sol .

#sent_id = 42
#text = Ils génèrent l' humus sur tous les continents de la Terre .

#sent_id = 43
#text = Dusty et moi , nous nous plaisons à dire que le dimanche , c' est là que nous allons à l' église .

#sent_id = 44
#text = Je suis amoureux des forêts anciennes , et je suis un patriote américain parce que nous en avons .

#sent_id = 45
#text = La plupart d' entre vous connaissez bien les champignons de Paris .

#sent_id = 46
#text = Et franchement , je suis face à un obstacle majeur :

#sent_id = 47
#text = J' espère donc faire éclater ce préjugé un fois pour tout avec votre groupe .

#sent_id = 48
#text = Nous appelons c' la mycophobie , la peur irrationnelle de l' inconnu quand il s' agit de champignons .

#sent_id = 49
#text = Les champignons ont une croissance très rapide .

#sent_id = 50
#text = Les champignons produisent de puissants antibiotiques .

#sent_id = 51
#text = En fait , nous sommes plus proches des champignons que de tout autre règne .

#sent_id = 52
#text = Nous partager les mêmes agents pathogène .

#sent_id = 53
#text = Les champignons n' aiment pas pourrir à cause de bactéries , et donc nos meilleurs antibiotiques viennent des champignons .

#sent_id = 54
#text = Mais voici un champignon qui n' est plus de première jeunesse .

#sent_id = 55
#text = Après avoir sporulé , ils pourrissent .

#sent_id = 56
#text = Mais je postule que la série de microbes qui apparaissent sur les champignons en train de pourrir sont essentiels pour la santé de la forêt .

#sent_id = 57
#text = Ils permettent aux arbres de pousser , ils créent les champs de débris qui nourrissent le mycélium .

#sent_id = 58
#text = Et donc , nous voyons ici un champignon en train de sporuler .

#sent_id = 59
#text = Et les spores germinent , et le mycélium se forme et aller dans le sous-sol .

#sent_id = 60
#text = Dans un seul pouce cube de sol , il peut y avoir plus de huit miles de ces cellules .

#sent_id = 61
#text = Mon pied couvre environ 300 miles de mycélium .

#sent_id = 62
#text = Voici des microphotographies de Nick Read et Patrick Hickey .

#sent_id = 63
#text = Et remarquez que pendant que le mycélium se développe , il conquérir le territoire et puis il commence à former un réseau .

#sent_id = 64
#text = Nous expirons de dioxyde de carbone , le mycélium aussi .

#sent_id = 65
#text = Il inhale de l' oxygène , tout comme nous .

#sent_id = 66
#text = Mais c' sont essentiellement des estomacs et des poumons externalisés .

#sent_id = 67
#text = Et je vous présente un concept selon lequel c' sont des membranes neurologique étendu .

#sent_id = 68
#text = Et dans ces cavités , ces microcavités se forment , et alors qu' elles fusionnent les sols , elles absorber de l' eau .

#sent_id = 69
#text = C' sont de petits puits .

#sent_id = 70
#text = Et à l' intérieur de ces puits , des communautés microbien commencent à se former ensuite .

#sent_id = 71
#text = J' ai d' abord avancé , au début des années 1990 , que le mycélium est l' internet naturel de la planète .

#sent_id = 72
#text = Quand vous regarder le mycélium , il est très ramifié .

#sent_id = 73
#text = Le mycélium est doué de sensations .

#sent_id = 74
#text = Il sait que vous êtes là .

#sent_id = 75
#text = Lorsque vous vous promenez à travers les paysages , il bondit dans les traces de vos pas en essayer de récupérer des débris .

#sent_id = 76
#text = Je pense donc que l' invention de l' internet des ordinateurs est une conséquence inévitable d' un modèle abouti antérieurement et biologiquement prouvé .

#sent_id = 77
#text = Si on pousse à l' extrême , la matière noire suit le même archétype mycéliaire .

#sent_id = 78
#text = Et c' est le paradigme que nous voyons dans tout l' univers .

#sent_id = 79
#text = La plupart d' entre vous ne savent peut-être pas que les champignons ont été les premiers organismes à venir sur la terre .

#sent_id = 80
#text = Ils sont venus sur la terre il y a 1,3 milliards d' années , et les plantes ont suivi plusieurs centaines de millions d' années plus tard .

#sent_id = 81
#text = Comment est c' possible ?

#sent_id = 82
#text = Il fait s' effriter les rochers , et c' est la première étape dans la production de sol .

#sent_id = 83
#text = L' acide oxalique est fait de deux molécules de dioxyde de carbone assemblées .

#sent_id = 84
#text = Ainsi , les champignons et le mycelium renferment de l' dioxyde de carbone sous la forme d' oxalates de calcium .

#sent_id = 85
#text = Et tout sortes d' autres oxalates renferment aussi de l' dioxyde de carbone par le biais des minéraux qui sont formés et sortis de la matrice rocheuse .

#sent_id = 86
#text = On a découvert cela pour la première fois en 1859 .

#sent_id = 87
#text = Voici une photographie de Franz Hueber .

#sent_id = 88
#text = Cette photographie a été prise en 1950 en Arabie Saoudite .

#sent_id = 89
#text = Il y a 420 millions d' années , c' organisme existait .

#sent_id = 90
#text = On l' a appelé Prototaxites .

#sent_id = 91
#text = Prototaxites , coucher , mesurait environ 90 cm de hauteur .

#sent_id = 92
#text = Les plus grandes plantes sur la Terre à l' époque mesuraient moins de 60 cm .

#sent_id = 93
#text = Ces champignons géant étaient disséminés sur toute la Terre .

#sent_id = 94
#text = Sur la plupart des terres émerger .

#sent_id = 95
#text = Et ils ont existé pendant des dizaines de millions d' années .

#sent_id = 96
#text = La terre a été frappée par un astéroïde , une énorme quantité de débris a été larguée dans l' atmosphère .

#sent_id = 97
#text = La lumière de l' soleil n' arrivait plus , et les champignons ont hérité de la Terre .

#sent_id = 98
#text = Les organismes qui s' étaient associés avec des champignons ont été récompensés , parce que les champignons n' ont pas besoin de lumière .

#sent_id = 99
#text = Ainsi , la perspective de champignons existante ailleurs sur d' autres planètes , est , je crois , courue d' avance ;

#sent_id = 100
#text = Au moins dans mon esprit .

#sent_id = 101
#text = Le plus grand organisme au monde est dans l' est de l' Oregon .

#sent_id = 102
#text = 2200 acres de superficie , 2000 ans d' age .

#sent_id = 103
#text = Le plus grand organisme de la planète est un tapis myceliaire , une paroi avec une seule cellule d' épaisseur .

#sent_id = 104
#text = Le mycélium , dans un bonnes conditions , produit un champignon - il traverse le sol avec une férocité telle qu' il peut briser l' asphalte .

#sent_id = 105
#text = Nous avons participé à plusieurs expériences .

#sent_id = 106
#text = Je vais vous montrer six , si je le peux , solutions pour aider à sauver le monde .

#sent_id = 107
#text = Les laboratoire Battelle et moi-même avons travaillé ensemble à Bellingham , dans l' état de Washington .

