from utils import file_to_list
from sacremoses import MosesTokenizer
from sacremoses import MosesDetokenizer
import argparse


def generate_template(filepath, lang, part, outpath, input_detok=True):
    """
    Input can be either detokenised, or tokenised. Write files to templates with "text=" for evaluation.
    Tokenise the input if not tokenised, and detokenise the input if tokenised.
    :param filepath:
    :param lang: language
    :param part: dev, dev_lemmas, test
    :param outpath:
    :param input_detok:
    :return:
    """
    lang_short = lang.split('_')[0] if '_' in lang else lang
    mosestok = MosesTokenizer(lang=lang_short)
    detokenizer = MosesDetokenizer(lang=lang_short)
    sents = file_to_list(filepath)
    templates = []
    templates_tok = []
    if 'ko' in lang:  # Korean
        # for predictions and gold don't do anything
        for iter, sent in enumerate(sents):
            templates.append(f'#sent_id = {iter + 1}\n#text = {sent}\n')
            templates_tok.append(f'#sent_id = {iter + 1}\n#text = {sent}\n')
    else:
        for iter, sent in enumerate(sents):
            # gold data
            if input_detok:
                tok_sent = mosestok.tokenize(sent, return_str=True, escape=False)
                if 'ru' in lang:
                    # special treatment for ru_gsd (aka ru_2)
                    tok_sent = tok_sent.replace('& # 39 ; & # 39 ;', '&#39;&#39;')
                templates.append(f'#sent_id = {iter + 1}\n#text = {sent}\n')
                templates_tok.append(f'#sent_id = {iter + 1}\n#text = {tok_sent}\n\n')
            # predictions, or tokens from ud (zh, ja, hi)
            else:
                # uppercase the first letter
                if len(sent) >= 1:
                    sent_first_cap = sent[0].upper() + sent[1:]
                elif len(sent) == 1:
                    sent_first_cap = sent[0].upper()
                else:
                    print('empty sentence', sent, iter + 1)
                    sent_first_cap = 'null'
                detok_sent = detokenizer.detokenize(sent_first_cap.split(), return_str=True)
                templates.append(f'#sent_id = {iter + 1}\n#text = {detok_sent}\n\n')
                templates_tok.append(f'#sent_id = {iter + 1}\n#text = {sent_first_cap}\n')

    if lang in ['zh', 'ja', 'ja_1', 'ja_2'] and not input_detok:
        pass
    else:
        # do not need detokenised files for WO evaluation with lemmas and MR from gold wo
        if part not in ['dev_lemmas', 'dev_from_gold_wo']:
            with open(f'{outpath}/{part}/{lang}_out.txt', 'w+') as f:
                f.write(''.join(templates))

    if lang in ['zh', 'ja', 'ja_1', 'ja_2', 'hi'] and input_detok:
        # we don't need detokenisation for those languages
        pass
    else:
        with open(f'{outpath}/{part}/{lang}_out.tok.txt', 'w+') as f:
            f.write(''.join(templates_tok))


def parse_args():
    parser = argparse.ArgumentParser(description='Detokenise predictions and put them into a template file;'
                                                 'or Tokenise gold data and put them into a template file.')

    parser.add_argument('inputfile', type=str,
                        help='path to a file to be processed')
    parser.add_argument('part', type=str, default='dev', choices=['dev', 'dev_lemmas', 'dev_from_gold_wo', 'test'],
                        help='on which part we test (default: dev)')
    parser.add_argument('lang', type=str,
                        help='language code (e.g. en, ar, pt, ru)')
    parser.add_argument('outdir', type=str,
                        help='path to a directory where a templated file will be written (e.g. /home/eval_dev_form/)')
    parser.add_argument('--detok_input', dest='detok_input', action='store_true',
                        help='input is detokenised')
    parser.add_argument('--token_input', dest='detok_input', action='store_false',
                        help='input is tokenised')
    parser.set_defaults(detokinput=True)
    args_init = parser.parse_args()
    return args_init


if __name__ == '__main__':
    args = parse_args()
    print('Processing prediction file', args.inputfile)
    generate_template(args.inputfile, args.lang, args.part, args.outdir, args.detok_input)
