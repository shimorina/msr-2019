from config import *
import json
from conllu import parse
from collections import defaultdict
from utils import read_conllu


"""
This code does the following:
read src files; for each node construct path-to-node representation; write it to other field of conllu
read src files; create delex ids for each node (det-1; det-2); write two types of delex dicts
"""


def tree_traverse_add_path(root, root_name):
    """
    Construct path-to-node representation for each node recursively and store it to the "other" field in conllu.
    :param root: tree node
    :param root_name: node tree path: deprel-lemma-deprel-lemma (e.g., ROOT-Al-root-kill-parataxis-force-nsubj)
    :return: tree conllu representation
    """
    lemma = root.token['form']
    if lemma == '_':
        pass
        # underscore is no problem, just a punctuation sign
        # print('Bad lemma; need to look at', root_name, root)
    root_name = root_name + '-' + lemma + '-' + root.token['deprel']
    root.token['other'] = root_name
    for child in root.children:
        new_root = tree_traverse_add_path(child, root_name)
    return root


def fully_anonymise_conllu(filepath, part, lang, dict_path):
    """
    Write two delexicalisation dictionaries.
    :param filepath: original conll file
    :param part: train, dev, test
    :param lang: language
    :param dict_path: path where to store delex dictionaries
    :return: None
    """
    fields = ('id', 'form', 'lemma', 'upostag', 'xpostag', 'feats', 'head', 'deprel', 'deps', 'misc', 'other')
    replace_dict_json = []  # each element is a line with a json dict
    dict_track_paths = []  # each element is a line with a dict {"path-to-root": (lemma, anonymised_lemma)}
    print(f'Processing {part} in {lang}')
    sentences = read_conllu(filepath)
    sentences_augmented = []
    for iterator, sent in enumerate(sentences):
        # add info to the "other" field
        sent = tree_traverse_add_path(sent, 'ROOT')
        sentences_augmented.append(sent)
        conll_data = sent.serialize()
        assert (len(parse(conll_data)) == 1), "Something wrong with the conllu format."

        replace_dict_json.append({})
        dict_track_paths.append({})
        replace_dict = defaultdict(int)  # keep track of ids for anonymised templates (e.g., DET-det-1)

        for token in parse(conll_data, fields)[0]:
            upos = token['upostag']
            anonymised_template = upos + '-' + token['deprel']
            replace_dict[anonymised_template] += 1
            anon_token = anonymised_template + '-' + str(replace_dict[anonymised_template])
            # pos=VERB,Gender=Masc,Number=Plur,Tense=Past,VerbForm=Part
            feats = token['feats']
            # feats.pop('lin', None) # todo: construct dicts without lin
            if feats:
                # escape ',' in values: e.g., replace VerbType=Aux,Cop with VerbType=Aux||Cop
                features = [k + '=' + v.replace(',', '||') for k, v in feats.items()]
                pos_feat = 'pos=' + upos + ','
                replace_dict_json[iterator][anon_token] = [token['form'], pos_feat + ','.join(features)]
            else:
                if lang in ['ja', 'ko_1', 'ko_2', 'zh']:  # todo: languages are hardcoded
                    replace_dict_json[iterator][anon_token] = [token['form'], 'pos=' + upos]
                else: # for punct signs, etc
                    replace_dict_json[iterator][anon_token] = [token['form'], '_']
            # print(token)
            path_to_root = token['other']
            # hack to handle "=" in path_to_root, since conllu interprets it as a dictionary
            if not isinstance(path_to_root, str):
                print('something wrong with the conllu reading')
            dict_track_paths[iterator][path_to_root] = [token['form'], anon_token]

    with open(dict_path + 'anonym-id-lemma-dict-' + lang + '-' + part + '.txt', 'w+', encoding='utf8') as f:
        for line in replace_dict_json:
            f.write(json.dumps(line, ensure_ascii=False) + '\n')
    with open(dict_path + 'anonym-path-full-dict-' + lang + '-' + part + '.txt', 'w+', encoding='utf8') as f:
        for line in dict_track_paths:
            f.write(json.dumps(line, ensure_ascii=False) + '\n')
    return sentences


# todo : original id!

def create_dicts():
    dict_path = GLOBAL_PATH + '/delex_dicts/'
    # test
    fully_anonymise_conllu(SHALLOW_TEST_FR_1, 'test', 'fr_1', dict_path)
    fully_anonymise_conllu(SHALLOW_TEST_FR_2, 'test', 'fr_2', dict_path)
    fully_anonymise_conllu(SHALLOW_TEST_FR_3, 'test', 'fr_3', dict_path)
    fully_anonymise_conllu(SHALLOW_TEST_ES_1, 'test', 'es_1', dict_path)
    fully_anonymise_conllu(SHALLOW_TEST_ES_2, 'test', 'es_2', dict_path)
    fully_anonymise_conllu(SHALLOW_TEST_RU_1, 'test', 'ru_1', dict_path)
    fully_anonymise_conllu(SHALLOW_TEST_RU_2, 'test', 'ru_2', dict_path)
    fully_anonymise_conllu(SHALLOW_TEST_AR, 'test', 'ar', dict_path)
    fully_anonymise_conllu(SHALLOW_TEST_EN_1, 'test', 'en_1', dict_path)
    fully_anonymise_conllu(SHALLOW_TEST_EN_2, 'test', 'en_2', dict_path)
    fully_anonymise_conllu(SHALLOW_TEST_EN_3, 'test', 'en_3', dict_path)
    fully_anonymise_conllu(SHALLOW_TEST_EN_4, 'test', 'en_4', dict_path)
    fully_anonymise_conllu(SHALLOW_TEST_ID, 'test', 'id', dict_path)
    fully_anonymise_conllu(SHALLOW_TEST_ZH, 'test', 'zh', dict_path)
    fully_anonymise_conllu(SHALLOW_TEST_KO_1, 'test', 'ko_1', dict_path)
    fully_anonymise_conllu(SHALLOW_TEST_KO_2, 'test', 'ko_2', dict_path)
    fully_anonymise_conllu(SHALLOW_TEST_PT_1, 'test', 'pt_1', dict_path)
    fully_anonymise_conllu(SHALLOW_TEST_PT_2, 'test', 'pt_2', dict_path)
    fully_anonymise_conllu(SHALLOW_TEST_ES_3, 'test', 'es_3', dict_path)
    fully_anonymise_conllu(SHALLOW_TEST_EN_5, 'test', 'en_5', dict_path)
    fully_anonymise_conllu(SHALLOW_TEST_EN_6, 'test', 'en_6', dict_path)
    fully_anonymise_conllu(SHALLOW_TEST_EN_7, 'test', 'en_7', dict_path)
    fully_anonymise_conllu(SHALLOW_TEST_RU_3, 'test', 'ru_3', dict_path)
    fully_anonymise_conllu(SHALLOW_TEST_KO_3, 'test', 'ko_3', dict_path)
    fully_anonymise_conllu(SHALLOW_TEST_PT_3, 'test', 'pt_3', dict_path)
    fully_anonymise_conllu(SHALLOW_TEST_HI_1, 'test', 'hi_1', dict_path)
    fully_anonymise_conllu(SHALLOW_TEST_HI_2, 'test', 'hi_2', dict_path)
    fully_anonymise_conllu(SHALLOW_TEST_JA_1, 'test', 'ja_1', dict_path)
    fully_anonymise_conllu(SHALLOW_TEST_JA_2, 'test', 'ja_2', dict_path)

    # train
    fully_anonymise_conllu(SHALLOW_TRAIN_FR_1, 'train', 'fr_1', dict_path)
    fully_anonymise_conllu(SHALLOW_TRAIN_FR_2, 'train', 'fr_2', dict_path)
    fully_anonymise_conllu(SHALLOW_TRAIN_FR_3, 'train', 'fr_3', dict_path)
    fully_anonymise_conllu(SHALLOW_TRAIN_ES_1, 'train', 'es_1', dict_path)
    fully_anonymise_conllu(SHALLOW_TRAIN_ES_2, 'train', 'es_2', dict_path)
    fully_anonymise_conllu(SHALLOW_TRAIN_RU_1, 'train', 'ru_1', dict_path)
    fully_anonymise_conllu(SHALLOW_TRAIN_RU_2, 'train', 'ru_2', dict_path)
    fully_anonymise_conllu(SHALLOW_TRAIN_AR, 'train', 'ar', dict_path)
    fully_anonymise_conllu(SHALLOW_TRAIN_EN_1, 'train', 'en_1', dict_path)
    fully_anonymise_conllu(SHALLOW_TRAIN_EN_2, 'train', 'en_2', dict_path)
    fully_anonymise_conllu(SHALLOW_TRAIN_EN_3, 'train', 'en_3', dict_path)
    fully_anonymise_conllu(SHALLOW_TRAIN_EN_4, 'train', 'en_4', dict_path)
    fully_anonymise_conllu(SHALLOW_TRAIN_ID, 'train', 'id', dict_path)
    fully_anonymise_conllu(SHALLOW_TRAIN_HI, 'train', 'hi', dict_path)
    fully_anonymise_conllu(SHALLOW_TRAIN_JA, 'train', 'ja', dict_path)
    fully_anonymise_conllu(SHALLOW_TRAIN_ZH, 'train', 'zh', dict_path)
    fully_anonymise_conllu(SHALLOW_TRAIN_KO_1, 'train', 'ko_1', dict_path)
    fully_anonymise_conllu(SHALLOW_TRAIN_KO_2, 'train', 'ko_2', dict_path)
    fully_anonymise_conllu(SHALLOW_TRAIN_PT_1, 'train', 'pt_1', dict_path)
    fully_anonymise_conllu(SHALLOW_TRAIN_PT_2, 'train', 'pt_2', dict_path)

    # dev
    fully_anonymise_conllu(SHALLOW_DEV_FR_1, 'dev', 'fr_1', dict_path)
    fully_anonymise_conllu(SHALLOW_DEV_FR_2, 'dev', 'fr_2', dict_path)
    fully_anonymise_conllu(SHALLOW_DEV_FR_3, 'dev', 'fr_3', dict_path)
    fully_anonymise_conllu(SHALLOW_DEV_ES_1, 'dev', 'es_1', dict_path)
    fully_anonymise_conllu(SHALLOW_DEV_ES_2, 'dev', 'es_2', dict_path)
    fully_anonymise_conllu(SHALLOW_DEV_RU_1, 'dev', 'ru_1', dict_path)
    fully_anonymise_conllu(SHALLOW_DEV_RU_2, 'dev', 'ru_2', dict_path)
    fully_anonymise_conllu(SHALLOW_DEV_AR, 'dev', 'ar', dict_path)
    fully_anonymise_conllu(SHALLOW_DEV_EN_1, 'dev', 'en_1', dict_path)
    fully_anonymise_conllu(SHALLOW_DEV_EN_2, 'dev', 'en_2', dict_path)
    fully_anonymise_conllu(SHALLOW_DEV_EN_3, 'dev', 'en_3', dict_path)
    fully_anonymise_conllu(SHALLOW_DEV_EN_4, 'dev', 'en_4', dict_path)
    fully_anonymise_conllu(SHALLOW_DEV_ID, 'dev', 'id', dict_path)
    fully_anonymise_conllu(SHALLOW_DEV_HI, 'dev', 'hi', dict_path)
    fully_anonymise_conllu(SHALLOW_DEV_JA, 'dev', 'ja', dict_path)
    fully_anonymise_conllu(SHALLOW_DEV_ZH, 'dev', 'zh', dict_path)
    fully_anonymise_conllu(SHALLOW_DEV_KO_1, 'dev', 'ko_1', dict_path)
    fully_anonymise_conllu(SHALLOW_DEV_KO_2, 'dev', 'ko_2', dict_path)
    fully_anonymise_conllu(SHALLOW_DEV_PT_1, 'dev', 'pt_1', dict_path)
    fully_anonymise_conllu(SHALLOW_DEV_PT_2, 'dev', 'pt_2', dict_path)
    fully_anonymise_conllu(SHALLOW_DEV_JA, 'dev', 'ja', dict_path)
    fully_anonymise_conllu(SHALLOW_DEV_ZH, 'dev', 'zh', dict_path)
    fully_anonymise_conllu(SHALLOW_DEV_KO_1, 'dev', 'ko_1', dict_path)
    fully_anonymise_conllu(SHALLOW_DEV_KO_2, 'dev', 'ko_2', dict_path)
    print(f'Dictionaries were written to {dict_path}.')


if __name__ == "__main__":
    create_dicts()
