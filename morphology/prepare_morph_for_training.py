import sys
sys.path.append('..')
from config import *
from utils import read_conllu_linear
import pyarabic.araby as araby


def ar_treatment(lemma):
    """Strip vowels from a text, including Shadda."""
    stripped_lemma = araby.strip_tashkeel(lemma)
    return stripped_lemma


def format_data(ud_file, lang, part):
    """
    Take original UDs, extract [lemma, MSD, form], form training, dev and test data
    Take original UD for train and dev; for test, take SR files (fields 'lemma' and 'form' are swapped).
    :param ud_file: conllu file
    :param lang: language code
    :param part: train, dev, test
    :return:
    """
    sentences = read_conllu_linear(ud_file)
    f_msd_l = []
    for sent in sentences:
        for line in sent:
            feats = line['feats']  # dict
            if feats:
                feats.pop('lin', None)  # delete 'lin' from test data
            # exclude punct signs and others with no morphology features
            if feats:
                if part != 'test':
                    lemma = line['lemma'].lower()
                    form = line['form'].lower()
                else:
                    lemma = line['form'].lower()
                    form = line['lemma'].lower()
                # exclude non-words, whose form is equal to lemma; we will copy them verbatim during inflection
                # but keep things like "1980 pos=NOUN,Number=Plur 1980s"
                if not lemma.isalpha() and lemma == form:
                    continue
                # exclude foreign words
                if 'Foreign' in feats.keys():
                    continue
                pos = line['upostag']
                # needed for identifying lang with no MSD
                '''if lang == 'ja' or lang == 'zh' or lang == 'ko':
                    if lemma != form:
                        print(lang, lemma, form, pos, feats)'''
                if lang == 'en' and '@card@' in lemma:
                    continue
                if lang == 'pt' and '_' in lemma:
                    continue
                if lang == 'fr' and ' ' in lemma:
                    continue
                if lang == 'ar':
                    lemma = ar_treatment(lemma)
                # dict to string: mood=Ind,Tense=Past,VerbForm=Fin
                # escape ',' in values: e.g., replace VerbType=Aux,Cop with VerbType=Aux||Cop
                features = [k + '=' + v.replace(',', '||') for k, v in feats.items()]
                pos_feat = 'pos=' + pos + ','
                f_msd_l.append((lemma, pos_feat + ','.join(features), form))
            else:
                if lang == 'ja' or lang == 'zh' or lang == 'ko':
                    if part != 'test':
                        lemma = line['lemma'].lower()
                        form = line['form'].lower()
                    else:
                        lemma = line['form'].lower()
                        form = line['lemma'].lower()
                    # exclude non-words, whose form is equal to lemma; we will copy them verbatim during inflection
                    # but keep things like "1980 pos=NOUN,Number=Plur 1980s"
                    if not lemma.isalpha() and lemma == form:
                        continue
                    pos = line['upostag']
                    f_msd_l.append((lemma, 'pos=' + pos + ',Feats=None', form))
    return f_msd_l


def process_several_files(files, lang, part, all_occurrences=True):
    """
    Read UD original files, extract morph info for each, merge into one output file.
    :param files: list of file names
    :param lang: language
    :param part: train, dev, test
    :param all_occurrences: True, if we want to have the same occurrence {lemma, msd, form} several times.
    All occurrences are needed to choose more frequent forms;
    False if no dublicate is needed (for test data)
    :return:
    """
    f_msd_l = []
    for f in files:
        f_msd_l = f_msd_l + format_data(f, lang, part)

    if not all_occurrences:
        f_msd_l = list(set(f_msd_l))
        prefix = 'one-occurr'
    else:
        prefix = 'all-occurr'

    output = []
    # construct lemma pos=N,case=ESS,num=PL form
    for element in f_msd_l:
        out = element[0] + '\t' + element[1] + '\t' + element[2] + '\n'
        output.append(out)

    with open(GLOBAL_PATH + '/morphology/data_for_reinflection/' + lang + '-task1-' + prefix + '-' + part, 'w+') as f:
        f.write(''.join(sorted(output)))
        print(f'{len(output)} instances for {lang} written to file')


def extract_data():
    # 1 en
    process_several_files([SHALLOW_TRAIN_UD_EN_1, SHALLOW_TRAIN_UD_EN_2, SHALLOW_TRAIN_UD_EN_3, SHALLOW_TRAIN_UD_EN_4], 'en', 'train')
    process_several_files([SHALLOW_DEV_UD_EN_1, SHALLOW_DEV_UD_EN_2, SHALLOW_DEV_UD_EN_3, SHALLOW_DEV_UD_EN_4], 'en', 'dev')
    process_several_files([SHALLOW_TEST_EN_1, SHALLOW_TEST_EN_2, SHALLOW_TEST_EN_3, SHALLOW_TEST_EN_4, SHALLOW_TEST_EN_5, SHALLOW_TEST_EN_6, SHALLOW_TEST_EN_7], 'en', 'test', all_occurrences=False)

    # 2 ar
    process_several_files([SHALLOW_TRAIN_UD_AR], 'ar', 'train')
    process_several_files([SHALLOW_DEV_UD_AR], 'ar', 'dev')
    process_several_files([SHALLOW_TEST_AR], 'ar', 'test', all_occurrences=False)

    # 3 es
    process_several_files([SHALLOW_TRAIN_UD_ES_1, SHALLOW_TRAIN_UD_ES_2], 'es', 'train')
    process_several_files([SHALLOW_DEV_UD_ES_1, SHALLOW_DEV_UD_ES_2], 'es', 'dev')
    process_several_files([SHALLOW_TEST_ES_1, SHALLOW_TEST_ES_2, SHALLOW_TEST_ES_3], 'es', 'test', all_occurrences=False)

    # 4 fr
    process_several_files([SHALLOW_TRAIN_UD_FR_1, SHALLOW_TRAIN_UD_FR_2, SHALLOW_TRAIN_UD_FR_3], 'fr', 'train')
    process_several_files([SHALLOW_DEV_UD_FR_1, SHALLOW_DEV_UD_FR_2, SHALLOW_DEV_UD_FR_3], 'fr', 'dev')
    process_several_files([SHALLOW_TEST_FR_1, SHALLOW_TEST_FR_2, SHALLOW_TEST_FR_3], 'fr', 'test', all_occurrences=False)

    # 5 id
    process_several_files([SHALLOW_TRAIN_UD_ID], 'id', 'train')
    process_several_files([SHALLOW_DEV_UD_ID], 'id', 'dev')
    process_several_files([SHALLOW_TEST_ID], 'id', 'test', all_occurrences=False)

    # 6 hi
    process_several_files([SHALLOW_TRAIN_UD_HI], 'hi', 'train')
    process_several_files([SHALLOW_DEV_UD_HI], 'hi', 'dev')
    process_several_files([SHALLOW_TEST_HI_1, SHALLOW_TEST_HI_2], 'hi', 'test', all_occurrences=False)

    # 9 pt
    process_several_files([SHALLOW_TRAIN_UD_PT_1, SHALLOW_TRAIN_UD_PT_2], 'pt', 'train')
    process_several_files([SHALLOW_DEV_UD_PT_1, SHALLOW_DEV_UD_PT_2], 'pt', 'dev')
    process_several_files([SHALLOW_TEST_PT_1, SHALLOW_TEST_PT_2, SHALLOW_TEST_PT_3], 'pt', 'test', all_occurrences=False)

    # 10 ru : todo separate two files?
    process_several_files([SHALLOW_TRAIN_UD_RU_1, SHALLOW_TRAIN_UD_RU_2], 'ru', 'train')
    process_several_files([SHALLOW_DEV_UD_RU_1, SHALLOW_DEV_UD_RU_2], 'ru', 'dev')
    process_several_files([SHALLOW_TEST_RU_1, SHALLOW_TEST_RU_2, SHALLOW_TEST_RU_3], 'ru', 'test', all_occurrences=False)

    for all_occurr in [True, False]:
        # Look at languages where MSD are poor
        # 11 zh : no morphology needed; get some rules from train+dev data
        process_several_files([SHALLOW_TRAIN_UD_ZH], 'zh', 'train', all_occurrences=all_occurr)
        process_several_files([SHALLOW_DEV_UD_ZH], 'zh', 'dev')
        process_several_files([SHALLOW_TEST_ZH], 'zh', 'test', all_occurrences=False)

        # 8 ko
        process_several_files([SHALLOW_TRAIN_UD_KO_1, SHALLOW_TRAIN_UD_KO_2], 'ko', 'train', all_occurrences=all_occurr)
        process_several_files([SHALLOW_DEV_UD_KO_1, SHALLOW_DEV_UD_KO_2], 'ko', 'dev')
        process_several_files([SHALLOW_TEST_KO_1, SHALLOW_TEST_KO_2, SHALLOW_TEST_KO_3], 'ko', 'test', all_occurrences=False)

        # 7 ja : no morphology needed
        process_several_files([SHALLOW_TRAIN_UD_JA], 'ja', 'train', all_occurrences=all_occurr)
        process_several_files([SHALLOW_DEV_UD_JA], 'ja', 'dev')
        process_several_files([SHALLOW_TEST_JA_1, SHALLOW_TEST_JA_2], 'ja', 'test', all_occurrences=False)


if __name__ == "__main__":
    extract_data()
