import json
from prepare_morph_based_on_freq import read_morph_file
import sys
sys.path.append('..')
from config import GLOBAL_PATH


def create_lemma_form_dict(filepath, lang, part):
    """ Create dicts for morphological inflection.
    After some testing, it has been found that for some langs using train, and then test is preferred.
    However, for some langs, using train data decreased the performance. So we mainly used only test predictions,
    rather than reused available morphological forms in train/dev data.
    :param filepath: prediction file from MR module
    :param lang: language code
    :param part: dev, test
    :return:
    """
    train_dev_path = GLOBAL_PATH + '/morphology/data_for_reinflection/'
    if lang not in ['ko', 'ja', 'zh']:
        morph_dict_test = read_morph_file(filepath)
    morph_dict_train = read_morph_file(train_dev_path + lang + '-task1-freq-train')
    morph_dict_dev = read_morph_file(train_dev_path + lang + '-task1-freq-dev')
    # create a dict with all possible forms from test, train, and dev:
    # the order matters; a wrong prediction from test will be overriden by {lemma,form} pair from train/dev
    if lang in ['en', 'it', 'fr']:
        full_dict = dict(morph_dict_test, **morph_dict_dev)
        full_dict.update(morph_dict_train)
    elif lang in ['ko', 'ja', 'zh']:
        full_dict = dict(morph_dict_dev, **morph_dict_train)
    else:
        full_dict = morph_dict_test
        # for testing different options for dict filling
        # full_dict = dict(morph_dict_dev, **morph_dict_train)
        # full_dict.update(morph_dict_test)
        # full_dict = dict(morph_dict_test, **morph_dict_dev)
        # full_dict.update(morph_dict_train)

    print('Length of final dict', len(full_dict), lang)
    full_dict_no_object = {}
    for k, v in full_dict.items():
        full_dict_no_object[k] = v.to_dict()
    assert len(full_dict_no_object) == len(full_dict)

    with open(GLOBAL_PATH + '/morphology/lemma_form_dicts/' + lang + '-' + part + '-dict.json', 'w+', encoding='utf8') as f:
        json.dump(full_dict_no_object, f, ensure_ascii=False)


def create_dicts_all_lang(morph_predictions_path, part):
    for lang in ['ar', 'id', 'hi', 'fr', 'ru', 'en', 'es', 'pt', 'ko', 'ja', 'zh']:
        morph_test_prediction = morph_predictions_path + '/' + lang + '-200-200-adam/' + lang + '.best.' + part + '.predictions'
        if part == 'test.test':
            short_part = 'test'
        else:
            short_part = part
        create_lemma_form_dict(morph_test_prediction, lang, short_part)


morph_predictions_path = GLOBAL_PATH + '/morphology/results-sr19/'
create_dicts_all_lang(morph_predictions_path, 'dev')
create_dicts_all_lang(morph_predictions_path, 'test.test')
# replace "j'" by "je" in the form for "je" manually in French after the dict generation todo
# todo possible improvement: look for form freq in train+dev, if > 2, take it, else use test
