import random
import sys
sys.path.append('..')
from config import GLOBAL_PATH


class Form:

    def __init__(self, features, lemma):
        self.features = features
        self.lemma = lemma
        self.possibleforms = []  # [form1, count], [form2, count]

    def choose_one_form(self, part='normal'):
        """
        Choose one form out of list of all possible forms based on their freq counts.
        If freq is equal => don't include those forms.
        :return: wordform or None
        """
        if len(self.possibleforms) == 1:
            return self.possibleforms[0][0]
        else:
            # decision based on freq
            freqs = [elem[1] for elem in self.possibleforms]
            if len(set(freqs)) == 1:
                if part != 'dual-test':  # todo: remove this, since we don't have this info during testing
                    return None
                else:  # no matter which form to take
                    return self.possibleforms[0][0]
            else:
                # choose the most frequent wordform
                return max(self.possibleforms, key=lambda x: x[1])[0]


class Entry:

    def __init__(self, lemma):
        self.lemma = lemma
        self.forms = {}  # feats: Form()

    def add_form(self, features, form):
        # wordform doesn't exist yet
        if features not in self.forms:
            form_obj = Form(features, self.lemma)
            form_obj.possibleforms.append([form, 1])
            self.forms[features] = form_obj
        # wordform already added; need to update counts
        else:
            possible_forms = self.forms[features].possibleforms
            found = False
            # update the count of the existent form
            for iterator, possible_form in enumerate(possible_forms):
                hyp_form, count = possible_form
                if form == hyp_form:
                    found = True
                    possible_forms[iterator][1] += 1  # increment count
                    break
            # add a new possible form
            if not found:
                possible_forms.append([form, 1])

    def to_dict(self):
        ord_dict = {}
        for form in self.forms.values():
            feats = form.features
            final_wordform = form.choose_one_form()
            ord_dict[feats] = final_wordform
        return ord_dict


def read_morph_file(filename):
    morph_dict = {}
    with open(filename, 'r') as f:
        for line in f:
            lemma, msd, form = line.split('\t')
            strip_form = form.strip()
            if strip_form:
                # morph_dict[lemma].update({msd: strip_form})
                if lemma not in morph_dict:
                    entry = Entry(lemma)
                else:
                    entry = morph_dict[lemma]
                entry.add_form(msd, strip_form)
                morph_dict[lemma] = entry
    print('items collected for morph dict', len(morph_dict))
    return morph_dict


def create_training_files_with_frequent_pairs(lang, part, dualforms=False):
    f_msd_l = []
    train_dev_path = GLOBAL_PATH + '/morphology/data_for_reinflection/'
    morph_dict = read_morph_file(train_dev_path + lang + '-task1-all-occurr-' + part)
    if dualforms:
        part = 'dual-' + part
    # check if there are more than 1 forms in possibleforms
    for entry in morph_dict.values():
        lemma = entry.lemma
        for feats, forms in entry.forms.items():
            wordform = forms.choose_one_form(part)
            if wordform:
                f_msd_l.append([lemma, feats, wordform])
    output = []
    for element in f_msd_l:
        out = element[0] + '\t' + element[1] + '\t' + element[2] + '\n'
        output.append(out)
    if part == 'dev':
        # choose 2000 examples randomly for dev
        part_short = 'short-dev'
        random.seed(10)
        output_short = random.sample(output, 2000)
        with open(GLOBAL_PATH + '/morphology/data_for_reinflection/' + lang + '-task1-freq-' + part_short, 'w+') as f:
            f.write(''.join(sorted(output_short)))
            print(f'{len(output_short)} {part} instances for {lang} written to file')

    with open(GLOBAL_PATH + '/morphology/data_for_reinflection/' + lang + '-task1-freq-' + part, 'w+') as f:
        f.write(''.join(sorted(output)))
        print(f'{len(output)} {part} instances for {lang} written to file')


def write_freq_data():
    create_training_files_with_frequent_pairs('en', 'train', False)
    create_training_files_with_frequent_pairs('en', 'dev', False)

    create_training_files_with_frequent_pairs('es', 'train', False)
    create_training_files_with_frequent_pairs('es', 'dev', False)

    create_training_files_with_frequent_pairs('fr', 'train', False)
    create_training_files_with_frequent_pairs('fr', 'dev', False)

    create_training_files_with_frequent_pairs('ar', 'train', False)
    create_training_files_with_frequent_pairs('ar', 'dev', False)

    create_training_files_with_frequent_pairs('id', 'train', False)
    create_training_files_with_frequent_pairs('id', 'dev', False)

    create_training_files_with_frequent_pairs('hi', 'train', False)
    create_training_files_with_frequent_pairs('hi', 'dev', False)

    create_training_files_with_frequent_pairs('pt', 'train', False)
    create_training_files_with_frequent_pairs('pt', 'dev', False)

    create_training_files_with_frequent_pairs('ru', 'train', False)
    create_training_files_with_frequent_pairs('ru', 'dev', False)

    create_training_files_with_frequent_pairs('ko', 'train', False)
    create_training_files_with_frequent_pairs('ko', 'dev', False)

    create_training_files_with_frequent_pairs('ja', 'train', False)
    create_training_files_with_frequent_pairs('ja', 'dev', False)

    create_training_files_with_frequent_pairs('zh', 'train', False)
    create_training_files_with_frequent_pairs('zh', 'dev', False)


if __name__ == "__main__":
    write_freq_data()
