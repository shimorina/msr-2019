from morphology.prepare_morph_based_on_freq import read_morph_file


def nice_output(instances, option, lang):
    output = []
    for count, inst in enumerate(instances):
        sorted_forms = sorted(inst[2], key=lambda x: x[1], reverse=True)
        nice_strings = [f'Form: {elem[0]}\tFreq: {elem[1]}' for elem in sorted_forms]
        out = f'{count + 1}.\nLemma: {inst[0]}\tMSD: {inst[1]}\n' + '\n'.join(nice_strings) + '\n'
        output.append(out)
    with open('./dual_forms/' + lang + '-' + option + '.txt', 'w+') as f:
        f.write('\n'.join(output))


def dual_form_identification(lang):
    train_dev_path = './data_for_reinflection/'
    # morph_dict_test = read_morph_file(filepath)
    morph_dict_train = read_morph_file(train_dev_path + lang + '-task1-all-occurr-train')
    # check if there are more than 1 forms in possibleforms
    dual_forms = []
    forms_with_equal_freq = []
    for entry in morph_dict_train.values():
        for feats, forms in entry.forms.items():
            if len(forms.possibleforms) > 1:
                dual_forms.append([entry.lemma, feats, forms.possibleforms])
                # equal frequency
                if len(set([elem[1] for elem in forms.possibleforms])) == 1:
                    forms_with_equal_freq.append([entry.lemma, feats, forms.possibleforms])
    nice_output(dual_forms, 'dual_forms', lang)
    nice_output(forms_with_equal_freq, 'eq_freq', lang)


def run_all():
    dual_form_identification('ja')
    dual_form_identification('ar')
    dual_form_identification('en')
    dual_form_identification('es')
    dual_form_identification('fr')
    dual_form_identification('hi')
    dual_form_identification('id')
    dual_form_identification('ko')
    dual_form_identification('pt')
    dual_form_identification('ru')
    dual_form_identification('zh')


if __name__ == "__main__":
    run_all()
